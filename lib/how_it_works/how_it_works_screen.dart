import 'package:flutter/material.dart';
import 'package:odaat_app/drawer/screens/drawer_screen.dart';
import 'package:odaat_app/edit_profile/screens/edit_profile_screen.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/widgets/custom_raised_button.dart';

class HowItWorks extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  HowItWorks({this.scaffoldKey});
  @override
  _HowItWorksState createState() => _HowItWorksState();
}

class _HowItWorksState extends State<HowItWorks> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.scaffoldKey == null ?
        (){
          AppNavigation.navigateReplacement(context,  DrawerScreen(menuItemNo: 1,));
    }
    :
     null,


      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AssetPaths.SPLASH_BACKGROUND_IMAGE),
                fit: BoxFit.fill
            )
        ),
        child: Scaffold(
          backgroundColor: AppColors.TRANSPARENT_COLOR,
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                howItWorksAppBar(),

                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(height: 15.0,),
                        Align(
                          alignment: Alignment.center,
                            child: Text(AppStrings.WORKS_NUM1,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 2.15,),
                        ),

                        Align(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.only(left: 20.0,right: 20.0,top: 4.0),
                              child: Text(AppStrings.WORKS_TEXT1,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400),textScaleFactor: 1.25,textAlign: TextAlign.center,)
                          ),
                        ),

                        SizedBox(height: 15.0,),
                        Align(
                          alignment: Alignment.center,
                          child: Text(AppStrings.WORKS_NUM2,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 2.15,),
                        ),

                        Align(
                          alignment: Alignment.center,
                          child: Padding(
                              padding: EdgeInsets.only(left: 20.0,right: 20.0,top: 4.0),
                              child: Text(AppStrings.WORKS_TEXT2,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400),textScaleFactor: 1.25,textAlign: TextAlign.center,)
                          ),
                        ),

                        SizedBox(height: 15.0,),
                        Align(
                          alignment: Alignment.center,
                          child: Text(AppStrings.WORKS_NUM3,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 2.15,),
                        ),

                        Align(
                          alignment: Alignment.center,
                          child: Padding(
                              padding: EdgeInsets.only(left: 20.0,right: 20.0,top: 4.0),
                              child: Text(AppStrings.WORKS_TEXT3,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400),textScaleFactor: 1.25,textAlign: TextAlign.center,)
                          ),
                        ),

                        SizedBox(height: 15.0,),
                        Align(
                          alignment: Alignment.center,
                          child: Text(AppStrings.WORKS_NUM4,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 2.15,),
                        ),

                        Align(
                          alignment: Alignment.center,
                          child: Padding(
                              padding: EdgeInsets.only(left: 20.0,right: 20.0,top: 4.0),
                              child: Text(AppStrings.WORKS_TEXT4,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400),textScaleFactor: 1.25,textAlign: TextAlign.center,)
                          ),
                        ),


                        SizedBox(height: 25.0,),

                        Padding(
                          padding: EdgeInsets.only(left: 18.0,right:18.0,top:15.0),
                          child: RichText(
                            text: TextSpan(
                              text: AppStrings.MARK_QUOTE_TEXT,
                              style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400,fontSize: 19.0,fontStyle: FontStyle.italic),
                              children: <TextSpan>[
                                TextSpan(text: AppStrings.MARK_TEXT, style: TextStyle(fontWeight: FontWeight.w800,fontStyle: FontStyle.normal)),

                              ],
                            ),
                          ),
                        ),

                        SizedBox(height: 10.0,),
                       widget.scaffoldKey == null ?  _nextWidget() : Container() ,
                        SizedBox(height: 10.0,),



                      ],

                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget howItWorksAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 5.0,right: 10.0,top:MediaQuery.of(context).size.height*0.03),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Spacer(
            flex: 1,
          ),

          SizedBox(width: 15.0,),

          Container(
            width: MediaQuery.of(context).size.width*0.5,
            height: MediaQuery.of(context).size.height*0.1,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(AssetPaths.HOW_IT_WORKS_IMAGE),
                    fit: BoxFit.fill
                )
            ),
          ),
          Spacer(
            flex: 1,
          ),
          widget.scaffoldKey == null ?
          IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.arrow_forward_ios,color: AppColors.WHITE_COLOR,),
            onPressed: ()
            {
              AppNavigation.navigateReplacement(context,  DrawerScreen(menuItemNo: 1,));
            },
          )
          :
          GestureDetector(
            onTap: (){
              //AppNavigation.navigateTo(context, HowItWorks());
              widget.scaffoldKey.currentState.openDrawer();
            },
            child: Image.asset(AssetPaths.MORE_ICON,width: 28.0,),
          )
        ],
      ),
    );
  }


  Widget _nextWidget()
  {
    return CustomRaisedButton(
        width:  MediaQuery.of(context).size.width*0.7,
        height: MediaQuery.of(context).size.height*0.062,
        text: AppStrings.NEXT_TEXT,
        onPressed: () {
          AppNavigation.navigateReplacement(context,  EditProfileScreen());

        }

    );
  }

}
