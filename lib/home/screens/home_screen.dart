import 'dart:convert';

import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:flutter/material.dart';
import 'package:odaat_app/services/firebase_messaging_service.dart';
import 'package:odaat_app/services/shared_preference.dart';
import 'package:odaat_app/set_goal/screens/add_goal_screen.dart';
import 'package:odaat_app/set_goal/screens/show_goal_screen.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_dialogs.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/widgets/custom_raised_button.dart';
import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:odaat_app/services/app_notification.dart';


class HomeScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  HomeScreen({this.scaffoldKey});
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  DatePickerController _datePickerController = DatePickerController();
  List<DragAndDropList> _contents = List<DragAndDropList>();
  List<dynamic> goalsList = List<dynamic>();
  TextEditingController _goalTitleController = TextEditingController();
  TextEditingController _goalMessageController = TextEditingController();
  DateFormat formatter = DateFormat('yyyy-MM-dd');
  DateTime goalDate = DateTime.now().add(Duration(days: 1));
  String goalDateFormat;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  DocumentSnapshot getDbGoals;
  String token;
  bool showIcons = true;
  AppNotification appNotification = AppNotification();
  List<DateTime> inActiveDates = List<DateTime>();

  String waitMessage = "Wait";

  List months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  String showCurrentDate;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    jumpToCurrentDate();

    showDateScreen(formatter.format(goalDate));


    appNotification.initializeNotification();

    goalDateFormat = formatter.format(goalDate);
    getGoalFromDb(goalDateFormat);



  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          homeAppBar(),
          Padding(
              padding: EdgeInsets.only(left: 23.0,right: 5.0,top: 10.0),
              child: GestureDetector(
                  onTap: (){
                  },
                  child: Text(showCurrentDate,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 1.3,)
              )
          ),

          Padding(
            padding: EdgeInsets.only(left: 23.0,right: 5.0,top: 10.0),
            child: calendarWidget(),
          ),

          Padding(
              padding: EdgeInsets.only(left: 23.0,right: 5.0,top: 10.0),
              child: Text(AppStrings.ACCOMPLISH_TOMORROW_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w500),textScaleFactor: 1.0,)
          ),

          SizedBox(
            height: 10.0,
          ),

          //Add Button
          Align(
            alignment: Alignment.centerRight,
            child: GestureDetector(
              onTap: (){
                //addGoalsDialog();
                addGoal();
              },
              child: goalsList.length <= 5  && goalDateFormat == formatter.format(DateTime.now().add(Duration(days:1))) ?
              Container(
                width:32.0,
                height:32.0,
                margin: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.05),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border:Border.all(color: AppColors.WHITE_COLOR),
                ),
                child: Center(
                  child: Icon(Icons.add,color:AppColors.WHITE_COLOR,size: 20.0,),
                ),
              )
                  :
                  Container(),
            ),
          ),

          SizedBox(
            height: 15.0,
          ),

          Expanded(
            child: waitMessage == "Wait"
            ?
            Column(
              children: [
                Align(
                  alignment:Alignment.bottomCenter,
                    child: AppDialogs.progressDialog()
                ),
              ],
            )
            :

            Container(
              padding: EdgeInsets.only(top:10.0),
              decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: AppColors.SHADOW_COLOR,
                      blurRadius: 5.0,
                      spreadRadius: 5.0,
                      offset: Offset(0.0, 0.0)
                  )
                ],
              ),
              child:DragAndDropLists(
                children: _contents,
                //onItemReorder: _onItemReorder,
                onListReorder: _onListReorder,
                listPadding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 2.0),
                itemDecorationWhileDragging: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 3,
                      offset: Offset(0, 0), // changes position of shadow
                    ),
                  ],
                ),

                lastItemTargetHeight: 10.0,
                //addLastItemTargetHeightToTop: true,
                //lastListTargetSize: 10.0,
                dragHandle:showIcons ?
                Padding(
                  padding: EdgeInsets.only(right: 5.0,top:12.0),
                  child: Icon(
                    Icons.menu,
                    color: AppColors.LIGHT_GREY_COLOR,
                  ),
                )
                    :
                    null,
              ),
            ),
          ),

          SizedBox(height: 10.0,),

          Align(
              alignment: Alignment.center,
              child: saveButton()
          ),

          SizedBox(height: 10.0,),
        ],
      ),
    );

  }


  Widget homeAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 5.0,right: 10.0,top:MediaQuery.of(context).size.height*0.03),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Spacer(),
          SizedBox(width: 15.0,),
          Text(AppStrings.ODAAT_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.7,),
          SizedBox(width: 10.0,),
          Spacer(),
          GestureDetector(
            onTap: (){
              //AppNavigation.navigateTo(context, HowItWorks());
              widget.scaffoldKey.currentState.openDrawer();
            },
            child: Image.asset(AssetPaths.MORE_ICON,width: 28.0,),
          )
        ],
      ),
    );
  }

  Widget calendarWidget()
  {
    return DatePicker(
      DateTime.now().add(Duration(days:1)).subtract(Duration(days: 100)),
      controller: _datePickerController,
      initialSelectedDate: goalDate,
      selectionColor: AppColors.YELLOW_COLOR,
      selectedTextColor: Colors.white,
      dateTextStyle: TextStyle(color: AppColors.WHITE_COLOR,fontSize: 14.0),
      monthTextStyle: TextStyle(color: AppColors.WHITE_COLOR,fontSize: 12.0),
      dayTextStyle: TextStyle(color: AppColors.WHITE_COLOR,fontSize: 12.0),
      daysCount: 111,
      inactiveDates: inActiveDatesList(),
      onDateChange: (date) {
        print("Date:"+date.toString());
        // New date selected
        goalsDate(date);
      },
    );
  }


  void jumpToCurrentDate() async
  {
    await Future.delayed(Duration(milliseconds: 300), () {
      _datePickerController.jumpToSelection();
    });

  }

  List<DateTime> inActiveDatesList()
  {
    inActiveDates = [];
    for(int i=1;i<=10;i++)
      {
        inActiveDates.add(DateTime.now().add(Duration(days:1)).add(Duration(days: i)));
      }

      return inActiveDates;
  }



  void goalsDragList()
  {
   _contents.clear();
    for(int i=0;i<goalsList.length;i++)
      {
        _contents.add(DragAndDropList(
          header: Row(
            children: [

              SizedBox(width:5.0),

              showIcons ?
              IconButton(
                  padding: EdgeInsets.zero,
                  icon: Icon(Icons.edit,color:AppColors.WHITE_COLOR,size: 20.0,),
                  constraints: BoxConstraints(),
                onPressed: (){
                    editGoalDialog(i);
                },
              )
              :
                  Container(),

              SizedBox(width:3.0),

              showIcons ?
              IconButton(
                  padding: EdgeInsets.zero,
                  icon: Icon(Icons.delete_forever,color:AppColors.WHITE_COLOR,size: 20.0,),
                  constraints: BoxConstraints(),
                onPressed: (){
                    deleteGoalDialog(i);
                },
              )
              :
              Container(),

              GestureDetector(
                onTap: (){
                  print("check man");
                  //AppNavigation.navigateTo(context,  SetGoalScreen());
                  AppNavigation.navigateTo(context,  ShowGoalScreen(goalList:goalsList,index:i,goalDate:formatter.format(goalDate),nextScreen:showIcons));

                },
                child:Container(
                  width: showIcons ? MediaQuery.of(context).size.width*0.65 : MediaQuery.of(context).size.width*0.8,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      border: Border.all(color: AppColors.WHITE_COLOR.withOpacity(0.9))
                  ),
                  margin: EdgeInsets.only(left: 5.0,right:5.0),
                  padding: EdgeInsets.only(left: 5.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                          child: Padding(
                            padding:EdgeInsets.only(right:5.0,top:15.0,bottom:15.0),
                              child: Text(goalsList[i]["goalTitle"],style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w500),textScaleFactor: 1.1,textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,)
                          )
                      ),
                      Container(
                          padding:EdgeInsets.only(right:10.0,left:10.0,top:15.0,bottom:15.0),
                          decoration: BoxDecoration(
                            color:Colors.brown[300],
                            borderRadius:BorderRadius.only(
                              topRight: Radius.circular(8.0),
                              bottomRight:Radius.circular(8.0),
                            ),
                              border: Border.all(color: AppColors.TRANSPARENT_COLOR)
                          ),
                          child: Text("${i+1}",style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w500),textScaleFactor: 1.1)
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )

        ));
      }

     setState(() {

     });

  }


  _onListReorder(int oldListIndex, int newListIndex) {
//    setState(() {
//
//
//    });

    var movedList = _contents.removeAt(oldListIndex);
    Map<String,String> data;
    print("OLD List index:"+oldListIndex.toString());
    print("NEW List index:"+newListIndex.toString());
    _contents.insert(newListIndex, movedList);
    data = goalsList[oldListIndex];

    goalsList.removeAt(oldListIndex);
    goalsList.insert(newListIndex, data);

    goalsDragList();

  }







  void addGoal()
  {
    print("Add goals");


   if(SharedPreference().getUser() != null) {
     Navigator.of(context)
         .push(MaterialPageRoute<Map<String,String>>(
       builder: (context) => AddGoalScreen(),
     ),).then((Map<String,String> goal) {
       if (goal != null) {
         goalsList.add(goal);
         goalsDragList();
       }
     });
   }

   else{
     addGoalDialogError();
   }
  }


  Widget saveButton()
  {


    return showIcons && goalDateFormat == formatter.format(DateTime.now().add(Duration(days:1)))?
      CustomRaisedButton(
      width:  MediaQuery.of(context).size.width*0.42,
      height: MediaQuery.of(context).size.height*0.062,
      text: AppStrings.SAVE_TEXT,
      onPressed: (){
        //AppNavigation.navigateTo(context,  SetGoalScreen());
        saveGoalDialog();
      },

    )
        :
        Container();
  }


  void addGoalDialogError()
  {
    {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Dialog(
              child: Container(
                decoration: BoxDecoration(
                  color: AppColors.PRIMARY_COLOR,
                  borderRadius: BorderRadius.circular(1.0),
                  border: Border(
                    top: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                    bottom: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                    left: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                    right: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),


                  ),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: 15.0,),
                    Text(AppStrings.ALERT_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),
                    Padding(
                        padding: EdgeInsets.only(left:25.0,right: 25.0,top: 15.0),
                        child: Text(AppStrings.GOAL_ERROR_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400,height: 1.7),textScaleFactor: 1.15,textAlign: TextAlign.center,)
                    ),

                    SizedBox(height: 15.0,),

                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          top: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.3)),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: (){
                              AppNavigation.navigatorPop(context);
                            },
                            child: Container(
                              width:MediaQuery.of(context).size.width*0.3,
                              color: AppColors.LIGHT_GREY_COLOR.withOpacity(0.6),
                              child: Padding(
                                  padding:EdgeInsets.only(top:10.0,bottom: 10.0),
                                  child: Text(AppStrings.OK_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.3,textAlign: TextAlign.center,)
                              ),
                            ),
                          ),
                        ],
                      ),
                    )

                  ],
                ),
              ),
            );
          });
    }
  }


  void editGoalDialog(int index)
  {
    _goalTitleController.text = goalsList[index]["goalTitle"];
    _goalMessageController.text = goalsList[index]["goalMessage"];
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            insetPadding: EdgeInsets.only(left: 15.0,right: 15.0),
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.circular(1.0),
                border: Border(
                  top: BorderSide(width: 1, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  bottom: BorderSide(width: 1, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  left: BorderSide(width: 1, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  right: BorderSide(width: 1, color: AppColors.WHITE_COLOR.withOpacity(0.2)),


                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 13.0,),
                  Text(AppStrings.EDIT_GOAL_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.bold),textScaleFactor: 1.9,),

                  SizedBox(height: 10.0,),

                  Padding(
                    padding: EdgeInsets.only(left:10.0,right: 10.0),
                    child: TextField(
                      controller: _goalTitleController,
                      style: TextStyle(color: AppColors.WHITE_COLOR),
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: AppColors.WHITE_COLOR,width: 1.0)
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: AppColors.WHITE_COLOR,width: 1.0)
                        ),
                        hintText: AppStrings.GOAL_TITLE_HINT,
                        hintStyle: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.7)),
                      ),
                      maxLines: 1,
                    ),
                  ),

                  SizedBox(height: 10.0,),
                  Padding(
                    padding: EdgeInsets.only(left:10.0,right: 10.0),
                    child: TextField(
                      controller: _goalMessageController,
                      style: TextStyle(color: AppColors.WHITE_COLOR),
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: AppColors.WHITE_COLOR,width: 1.0)
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: AppColors.WHITE_COLOR,width: 1.0)
                        ),
                        hintText: AppStrings.GOAL_TITLE_HINT,
                        hintStyle: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.7)),
                      ),
                      maxLines: 3,
                    ),
                  ),


                  SizedBox(height: 10.0,),

                  GestureDetector(
                    onTap: (){
                      editGoal(index);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.LIGHT_GREY_COLOR.withOpacity(0.6),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                          padding:EdgeInsets.only(top:5.0,bottom: 5.0,left:20.0,right:20.0),
                          child: Text(AppStrings.ADD_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.2,textAlign: TextAlign.center,)
                      ),
                    ),
                  ),

                  SizedBox(height: 10.0,),

                ],
              ),
            ),
          );
        });
  }




  void deleteGoalDialog(int index)
  {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.circular(1.0),
                border: Border(
                  top: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  bottom: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  left: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  right: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),


                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 15.0,),
                  Text(AppStrings.ALERT_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),
                  Padding(
                      padding: EdgeInsets.only(left:25.0,right: 25.0,top: 15.0),
                      child: Text(AppStrings.DELETE_ASK_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400,height: 1.7),textScaleFactor: 1.15,textAlign: TextAlign.center,)
                  ),

                  SizedBox(height: 15.0,),

                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.3)),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              AppNavigation.navigatorPop(context);
                              deleteGoals(index);

                            },
                            child: Padding(
                                padding:EdgeInsets.only(top:13.0,bottom: 13.0),
                                child: Text(AppStrings.YES_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.2,textAlign: TextAlign.center,)
                            ),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              AppNavigation.navigatorPop(context);
                            },
                            child: Container(
                              color: AppColors.LIGHT_GREY_COLOR.withOpacity(0.6),
                              child: Padding(
                                  padding:EdgeInsets.only(top:13.0,bottom: 13.0),
                                  child: Text(AppStrings.NO_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.2,textAlign: TextAlign.center,)
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )

                ],
              ),
            ),
          );
        });
  }


  void saveGoalDialog()
  {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.circular(1.0),
                border: Border(
                  top: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  bottom: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  left: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                  right: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),


                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 15.0,),
                  Text(AppStrings.REMINDER_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),
                  Padding(
                      padding: EdgeInsets.only(left:25.0,right: 25.0,top: 15.0),
                      child: Text(AppStrings.SAVE_GOAL_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400,height: 1.7),textScaleFactor: 1.15,textAlign: TextAlign.center,)
                  ),

                  SizedBox(height: 15.0,),

                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.3)),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              AppNavigation.navigatorPop(context);
                              saveGoals();

                            },
                            child: Padding(
                                padding:EdgeInsets.only(top:13.0,bottom: 13.0),
                                child: Text(AppStrings.YES_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.2,textAlign: TextAlign.center,)
                            ),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              AppNavigation.navigatorPop(context);
                            },
                            child: Container(
                              color: AppColors.LIGHT_GREY_COLOR.withOpacity(0.6),
                              child: Padding(
                                  padding:EdgeInsets.only(top:13.0,bottom: 13.0),
                                  child: Text(AppStrings.NO_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.2,textAlign: TextAlign.center,)
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )

                ],
              ),
            ),
          );
        });
  }




  void editGoal(int index)
  {
    goalsList[index]["goalTitle"] = _goalTitleController.text;
    goalsList[index]["goalMessage"] = _goalMessageController.text;
    _goalTitleController.text = "";
    _goalMessageController.text = "";
    goalsDragList();
    AppNavigation.navigatorPop(context);
  }


  void deleteGoals(int i)
  {
 goalsList.removeAt(i);
 goalsDragList();
  print(i.toString());
  }



  void goalsDate(DateTime date)
  {
    goalDate = date;

    goalDateFormat = formatter.format(goalDate);
    getGoalFromDb(goalDateFormat);
    print(date.toString());
  }

  void saveGoals() async
  {

    if(goalsList.length == 6) {

      AppDialogs.progressAlertDialog(context: context);

      goalDateFormat = formatter.format(goalDate);

      token = await FirebaseMessagingService().getToken();

      print("Token:${token}");
      print(goalDateFormat);


      //Insert goals in database
      users.doc(token).collection("user_goals").doc(goalDateFormat)
          .set({"goals": goalsList})
          .then((value) {
        //goalsList.clear();
            AppNavigation.navigateCloseDialog(context);
            AppDialogs.showToast(message: AppStrings.GOAL_ADD_CONGRATULATIONS_TEXT);

            //To set goals in shared preference if app closed then user see goals
            SharedPreference().setGoalList(goalList:jsonEncode({"goals": goalsList}));
            SharedPreference().setCurrentGoalIndex(index:0);
            SharedPreference().setGoalDate(date:goalDateFormat);

            appNotification.addedGoalNotification(title: "Congratulations!",body: "Goals Added For Date:${goalDateFormat}");



            showIcons = false;

            goalsDragList();


          })
           .catchError((error){
             print("Failed to add user: $error");
             AppNavigation.navigateCloseDialog(context);
             AppDialogs.showToast(message: error.message);
           });


      print("savve goals" + goalsList.toString());
      //
    }

    else
      {
        AppDialogs.showToast(message: "Please enter six goals first");
      }
  }



  void getGoalFromDb(String savedGoalDate) async
  {
    showDateScreen(savedGoalDate);

    waitMessage = "Wait";

    setState(() {

    });

    token = await FirebaseMessagingService().getToken();

     getDbGoals = await users.doc(token)
        .collection("user_goals")
        .doc(savedGoalDate)
        .get();

  print("Ab map:"+getDbGoals.data().toString());

  if(getDbGoals.data() != null)
    {
      goalsList.clear();
      //print("Ab list:"+getDbGoals.data()["goals"].toString());
      //changes here
      dynamic goalObject;
      goalObject = getDbGoals.data();
      goalsList = goalObject["goals"];
      showIcons = false;
      waitMessage = "No Wait";
      goalsDragList();


    }
    else
      {
        goalsList.clear();
        showIcons = true;
        waitMessage = "No Wait";
        goalsDragList();
      }



    //goalsList.add(_goalController.text);

    //_goalController.text = "";
    //goalsDragList();

  }



  void showDateScreen(String currentDate)
  {
    showCurrentDate = months[int.parse(currentDate.substring(5,7)) - 1] + ", " +currentDate.substring(0,4);


  setState(() {

  });


  }


}
