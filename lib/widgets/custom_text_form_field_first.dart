import 'package:flutter/material.dart';
import 'package:odaat_app/utils/app_colors.dart';

class CustomTextFormFieldFirst extends StatelessWidget {

  String hintText;
  TextEditingController controller = TextEditingController();
  Function validator;
  int maxLines;

  CustomTextFormFieldFirst({this.hintText,this.controller,this.validator,this.maxLines});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 18.0,right:18.0,top:5.0),
      child: TextFormField(
        controller: controller,
        validator: validator,
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
              borderRadius: BorderRadius.circular(5.0),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.RED_COLOR),
              borderRadius: BorderRadius.circular(5.0),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.RED_COLOR),
              borderRadius: BorderRadius.circular(5.0),
            ),
            hintText: hintText,
            hintStyle: new TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.6),fontSize: 16.0),
            fillColor: AppColors.LIGHT_GREY_COLOR,
            filled: true,
            contentPadding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 16.0)
        ),

        style: TextStyle(
          fontSize: 16.0,
          color: AppColors.WHITE_COLOR.withOpacity(0.9),
        ),
        maxLines: maxLines ?? 1,
      ),
    );
  }
}
