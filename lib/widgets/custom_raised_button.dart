import 'package:flutter/material.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_strings.dart';


class CustomRaisedButton extends StatelessWidget {
  double width,height;
  String text;
  Function onPressed;

  CustomRaisedButton({this.width,this.height,this.text,this.onPressed});



  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: RaisedButton(
        onPressed: onPressed,
        color: AppColors.PRIMARY_COLOR,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
            side: BorderSide(color: AppColors.WHITE_COLOR,width: 1.3),
        ),
        child: Text(text,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w800),textScaleFactor: 1.20,),
      ),
    );
  }
}
