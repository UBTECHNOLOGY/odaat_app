import 'package:flutter/material.dart';



import 'package:flutter/material.dart';
import 'package:odaat_app/utils/app_colors.dart';

class CustomTextFormFieldSecond extends StatelessWidget {

  String hintText;
  TextEditingController controller = TextEditingController();
  Function validator,onTap;
  bool readOnly;

  CustomTextFormFieldSecond({this.hintText,this.controller,this.validator,this.readOnly,this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 25.0,right:25.0,top: 5.0),
      child: TextFormField(
        controller: controller,
        validator: validator,
        readOnly: readOnly ?? false,
        onTap: onTap,
        decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.LIGHT_GREY_COLOR,width: 2.0),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.LIGHT_GREY_COLOR,width: 2.0),

            ),
            hintText: hintText,
            hintStyle: new TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.6),fontSize: 16.0),
            contentPadding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 8.0),
        ),
        textAlign: TextAlign.center,

        style: TextStyle(
          fontSize: 16.0,
          color: AppColors.WHITE_COLOR.withOpacity(0.9),

        ),
      ),
    );
  }
}
