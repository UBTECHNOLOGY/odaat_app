import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:odaat_app/services/shared_preference.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_dialogs.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/support_us/blocs/stripe_payment_bloc.dart';
import 'package:odaat_app/support_us/blocs/rating_bloc.dart';
import 'package:odaat_app/utils/navigation.dart';

class SupportUsScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  SupportUsScreen({this.scaffoldKey});
  @override
  _SupportUsScreenState createState() => _SupportUsScreenState();
}

class _SupportUsScreenState extends State<SupportUsScreen> {

  StripePaymentBloc stripePaymentBloc = StripePaymentBloc();
  TextEditingController _otherAmountController = TextEditingController();
  
  RatingBloc ratingBloc = RatingBloc();
  
 String userName=AppStrings.USER_NAME_TEXT;
  String profileNetworkImage = null;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserData();
    stripePaymentBloc.initializePaymentOptions();
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          supportUsAppBar(),
          SizedBox(height:10.0),
          supportUsRow(),

          Expanded(
            child: Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.03),
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.03),
              decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: AppColors.SHADOW_COLOR,
                      blurRadius: 4.0,
                      spreadRadius: 5.0,
                      offset: Offset(0.0, 0.0)
                  )
                ],
              ),
              child:
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Padding(
                      padding: EdgeInsets.only(left: 15.0,right: 40.0),
                      child:Text(AppStrings.WANT_SUPPORT_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500,height: 1.5),textScaleFactor: 1.2,),

                    ),

                    SizedBox(height:15.0),

                    Padding(
                      padding: EdgeInsets.only(left: 15.0,right: 40.0),
                      child:Text(AppStrings.FUNDS_BETTER_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500,height: 1.5),textScaleFactor: 1.2,),
                    ),

                    SizedBox(height:40.0),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        payContainer(AppStrings.FIVE_DOLLAR_TEXT,1,"5"),
                        payContainer(AppStrings.TEN_DOLLAR_TEXT,2,"10"),
                        payContainer(AppStrings.TWENTY_DOLLAR_TEXT,3,"20"),
                        payContainer(AppStrings.OTHER_TEXT,4,"0"),
                      ],
                    ),

                    SizedBox(height:40.0),

                    Padding(
                      padding: EdgeInsets.only(left: 15.0,right: 15.0),
                      child:Text(AppStrings.RATING_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500,height: 1.6),textScaleFactor: 1.2,),
                    ),

                    SizedBox(height:40.0),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap:()
                          {
                            giveReview();
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width*0.35,
                            height: MediaQuery.of(context).size.height*0.055,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(AssetPaths.ANDROID_STORE_IMAGE),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),

                        GestureDetector(
                          onTap: (){
                            giveReview();
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width*0.35,
                            height: MediaQuery.of(context).size.height*0.055,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(AssetPaths.APPLE_STORE_IMAGE),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),

                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }


  Widget supportUsAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 15.0,right: 10.0,top:MediaQuery.of(context).size.height*0.03),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Container(
            width: 60.0,
            height: 60.0,
            margin: EdgeInsets.only(top: 10.0),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border:Border.all(color: AppColors.WHITE_COLOR,width: 2.0),
                image: DecorationImage(
                    image: profileNetworkImage != null ?
                    NetworkImage(profileNetworkImage)
                        :

                    AssetImage(AssetPaths.USER_PROFILE_MENU_IMAGE),
                    fit: BoxFit.fill
                )
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top:10.0,left: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(AppStrings.GOOD_MORNING_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400),textScaleFactor: 1.0,),
                SizedBox(height: 5.0,),
                Text(userName,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w700),textScaleFactor: 1.0,),
              ],
            ),
          ),

          Spacer(),
          GestureDetector(
            onTap: (){
              widget.scaffoldKey.currentState.openDrawer();
            },
            child: Image.asset(AssetPaths.MORE_ICON,width: 28.0,),
          )
        ],
      ),
    );
  }



  Widget supportUsRow()
  {
    return Padding(
      padding: EdgeInsets.only(left: 15.0,top:10.0),
      child: Row(
        children: [
          Image.asset(AssetPaths.SUPPORT_US_IMAGE,width: 40.0,),
          SizedBox(width:15.0),
          Text(AppStrings.SUPPORT_US_TEXT.toUpperCase(),style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),
        ],
      ),
    );
  }


  Widget payContainer(String text,int index,String amount)
  {
    return GestureDetector(
      onTap: (){

        print("Amount:${amount}");
        if(index == 4)
          {
            otherAmountDialog();
          }
          else
            {
              payAmount(amount);
            }

      },
      child: Container(
        width: MediaQuery.of(context).size.width*0.18,
        padding: EdgeInsets.only(top: 7.0,bottom: 7.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7.0),
          border: Border.all(color: AppColors.WHITE_COLOR),
        ),

        child:Text(text,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 1.0,textAlign: TextAlign.center,),
      ),
    );

  }



  void otherAmountDialog()
  {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            insetPadding: EdgeInsets.only(left: 15.0,right: 15.0),
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.circular(1.0),
                border: Border(
                  top: BorderSide(width: 1, color: AppColors.WHITE_COLOR.withOpacity(0.5)),
                  bottom: BorderSide(width: 1, color: AppColors.WHITE_COLOR.withOpacity(0.5)),
                  left: BorderSide(width: 1, color: AppColors.WHITE_COLOR.withOpacity(0.5)),
                  right: BorderSide(width: 1, color: AppColors.WHITE_COLOR.withOpacity(0.5)),


                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 13.0,),
                  SizedBox(height: 10.0,),

                  Padding(
                    padding: EdgeInsets.only(left:10.0,right: 10.0),
                    child: TextField(
                      controller: _otherAmountController,
                      style: TextStyle(color: AppColors.WHITE_COLOR),
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppColors.WHITE_COLOR,width: 1.0)
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppColors.WHITE_COLOR,width: 1.0)
                        ),
                        hintText: AppStrings.ENTER_AMOUNT_HINT,
                        hintStyle: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.7)),
                      ),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                    ),
                  ),

                  SizedBox(height: 25.0,),

                  GestureDetector(
                    onTap: (){
//                      addGoal();

                    if(_otherAmountController.text.isNotEmpty)
                      {
                        AppNavigation.navigatorPop(context);
                         payAmount( _otherAmountController.text);
                      }
                     else
                       {
                         AppDialogs.showToast(message: AppStrings.PLEASE_ENTER_AMOUNT);
                       }

                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.LIGHT_GREY_COLOR.withOpacity(0.6),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                          padding:EdgeInsets.only(top:5.0,bottom: 5.0,left:20.0,right:20.0),
                          child: Text(AppStrings.PAY_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.2,textAlign: TextAlign.center,)
                      ),
                    ),
                  ),

                  SizedBox(height: 10.0,),

                ],
              ),
            ),
          );
        });
  }


  void payAmount(String amount)
  {
    stripePaymentBloc.payWithNewCard(amount:amount,currency: "USD",context: context);
  }


  
  void giveReview()
  {
    print("review");
    ratingBloc.provideRating(context);
  }


  void getUserData()
  {
    if(SharedPreference().getUser() != null)
    {
      userName = jsonDecode(SharedPreference().getUser())["name"];
      profileNetworkImage = jsonDecode(SharedPreference().getUser())["profileImage"];


    }
  }
  
}
