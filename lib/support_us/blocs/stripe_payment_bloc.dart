import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:odaat_app/utils/app_dialogs.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:stripe_payment/stripe_payment.dart';


class StripePaymentBloc {
  String paymentApiUrl = '${AppStrings.API_BASE}/payment_intents';
  Map<String, String> headers = {
    'Authorization': 'Bearer ${AppStrings.SECRET_KEY}',
    'Content-Type': 'application/x-www-form-urlencoded'
  };

  PaymentMethod paymentMethod;

  Map<String,dynamic> paymentIntent;

  PaymentIntentResult response;

  void initializePaymentOptions() {
    StripePayment.setOptions(
        StripeOptions(
            publishableKey: AppStrings.PUBLISHABLE_KEY,
            //merchantId: "Test",
            androidPayMode: 'test'
        )
    );
  }


  void payWithNewCard({String amount, String currency,BuildContext context}) async {

    try {

      paymentMethod = await StripePayment.paymentRequestWithCardForm(
          CardFormPaymentRequest()
      );

      paymentIntent = await createPaymentIntent(
          (int.parse(amount) * 100).toString(),
          currency,
          context
      );

      response = await StripePayment.confirmPaymentIntent(
          PaymentIntent(
              clientSecret: paymentIntent['client_secret'],
              paymentMethodId: paymentMethod.id
          )
      );

      if (response.status == 'succeeded') {
        AppDialogs.showToast(message: AppStrings.TRANSACTION_SUCCESSFULL);

      }
      else {
        AppDialogs.showToast(message: AppStrings.TRANSACTION_FAILED);
      }


      AppNavigation.navigateCloseDialog(context);

    }
    on PlatformException catch(err) {
      if (err.code == 'cancelled') {
        AppDialogs.showToast(message: AppStrings.TRANSACTION_CANCELLED);
      }
    else
      {
        AppDialogs.showToast(message: AppStrings.WENT_WRONG);
        AppNavigation.navigateCloseDialog(context);
      }

    } catch (err) {
      AppDialogs.showToast(message: AppStrings.TRANSACTION_FAILED + ":${err.toString()}");
      AppNavigation.navigateCloseDialog(context);
    }



  }


   Future<Map<String, dynamic>> createPaymentIntent(String amount, String currency,BuildContext context) async {

    AppDialogs.progressAlertDialog(context: context);

    try {
      Map<String, dynamic> body = {
        'amount': amount,
        'currency': currency,
        'payment_method_types[]': 'card'
      };
      var response = await post(
          Uri.parse(paymentApiUrl),
          body: body,
          headers: headers
      );
      return jsonDecode(response.body);
    } catch (err) {
      print('err charging user: ${err.toString()}');
    }
    return null;
  }
}
