import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:odaat_app/drawer/screens/drawer_screen.dart';
import 'package:odaat_app/services/firebase_messaging_service.dart';
import 'package:odaat_app/services/shared_preference.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_dialogs.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:analog_clock/analog_clock.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:intl/intl.dart';

class CurrentGoalScreen extends StatefulWidget {
  List setGoalList;
  int setGoalIndex;
  String setGoalDate;

  CurrentGoalScreen({this.setGoalList,this.setGoalIndex,this.setGoalDate});


  @override
  _CurrentGoalScreenState createState() => _CurrentGoalScreenState();
}

class _CurrentGoalScreenState extends State<CurrentGoalScreen> {
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  bool goalSet = false;
  Map<int,String> goalNum = {
    0:"1st",
    1:"2nd",
    2:"3rd",
    3:"4th",
    4:"5th",
    5:"6th",
  };
  String goalTime="00:00:00",token;

  List goalList;
  int goalIndex;
  String goalDateFormat;
  Duration dateDifference;
  int increaseGoalIndex;

  String userName=AppStrings.USER_NAME_TEXT;
  String profileNetworkImage = null;
Timer goalTimer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    goalList = widget.setGoalList;
    goalIndex = widget.setGoalIndex;
    goalDateFormat = widget.setGoalDate;

    print("Goal List:"+goalList.toString());
    print("Goal Index:"+goalIndex.toString());
    print("Goal Date:"+goalDateFormat.toString());

    getUserData();
    showTime();

  }


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AssetPaths.SPLASH_BACKGROUND_IMAGE),
              fit: BoxFit.fill
          )
      ),
      child: Scaffold(
        backgroundColor: AppColors.TRANSPARENT_COLOR,
        body: SafeArea(
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              currentGoalAppBar(),
              SizedBox(height: 10.0,),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 12.0),
                    child: Text(goalIndex > 0 ?  AppStrings.CONGRATULATIONS_TEXT : AppStrings.HEY_WINNER_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w900),textScaleFactor: 1.7,),
                ),
              ),

              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.03),
                  margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.03),
                  width: MediaQuery.of(context).size.width*1.0,
                  decoration: BoxDecoration(
                    color: AppColors.PRIMARY_COLOR,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppColors.SHADOW_COLOR,
                          blurRadius: 4.0,
                          spreadRadius: 5.0,
                          offset: Offset(0.0, 0.0)
                      )
                    ],
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Padding(
                          padding: EdgeInsets.only(left: 15.0,right: 10.0,),
                          child:RichText(
                            text: TextSpan(
                              text: AppStrings.YOUR_TEXT,
                              style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w300,fontSize: 20.0),
                              children: <TextSpan>[
                                TextSpan(text: goalNum[goalIndex], style: TextStyle(fontWeight: FontWeight.w800,fontStyle: FontStyle.normal)),
                                TextSpan(text: AppStrings.GOAL_TEXT),

                              ],
                            ),
                          )
                  ),

                        Padding(
                            padding: EdgeInsets.only(left: 15.0,right: 10.0,top: 13.0),
                            child: Text("\"${goalList[goalIndex]["goalMessage"]}\"",style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w800),textScaleFactor: 1.6,)
                        ),



                        SizedBox(height:30.0),

                        timerScreen(),

                        SizedBox(height:35.0),


                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            goalList[goalIndex]["status"] == "pending" ?
                            Column(
                              children: [
                                GestureDetector(
                                  onTap:()
                                  {
                                    changeGoalStatus(setStatus:"started");
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width*0.44,
                                    height: MediaQuery.of(context).size.height*0.09,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(AssetPaths.PAUSE_IMAGE),
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 10.0),
                                  child: Text(AppStrings.START_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 1.2,),
                                ),


                              ],
                            )
                            :
                            Container(),

                            goalList[goalIndex]["status"] == "pending" || goalList[goalIndex]["status"] == "started" ?
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: (){
                                    completeDialog();
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width*0.44,
                                    height: MediaQuery.of(context).size.height*0.09,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(AssetPaths.COMPLETE_IMAGE),
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 10.0),
                                  child: Text(AppStrings.COMPLETE_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 1.2,),
                                ),


                              ],
                            )
                                :
                            Container(),
                          ],
                        ),


                        SizedBox(height:15.0),

                        Padding(
                          padding: EdgeInsets.only(left: 18.0,right:18.0,top:15.0),
                          child: RichText(
                            text: TextSpan(
                              text: AppStrings.MARK_QUOTE_TEXT,
                              style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400,fontSize: 18.0,fontStyle: FontStyle.italic),
                              children: <TextSpan>[
                                TextSpan(text: AppStrings.MARK_TEXT, style: TextStyle(fontWeight: FontWeight.w800,fontStyle: FontStyle.normal)),

                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget currentGoalAppBar() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 2.0, top: MediaQuery
          .of(context)
          .size
          .height * 0.05),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Container(
            width: 60.0,
            height: 60.0,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: profileNetworkImage != null ?
                    NetworkImage(profileNetworkImage)
                        :

                    AssetImage(AssetPaths.USER_PROFILE_MENU_IMAGE),
                    fit: BoxFit.fill
                )
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: 10.0, left: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(AppStrings.GOOD_MORNING_TEXT, style: TextStyle(
                    color: AppColors.WHITE_COLOR.withOpacity(0.8),
                    fontWeight: FontWeight.w400), textScaleFactor: 1.0,),
                SizedBox(height: 5.0,),
                Text(userName.toString(), style: TextStyle(
                    color: AppColors.WHITE_COLOR, fontWeight: FontWeight.w700),
                  textScaleFactor: 1.0,),
              ],
            ),
          ),

          Spacer(),
          IconButton(
            icon: Icon(Icons.arrow_forward_ios,color: AppColors.WHITE_COLOR,),
            padding: EdgeInsets.zero,
            onPressed: ()
            {
              AppNavigation.navigateReplacement(
                  context, DrawerScreen(menuItemNo: 1,));
            },
          ),

        ],
      ),
    );
  }


  Widget timerScreen()
  {
       return Container(
         width:150.0,
         height:150.0,
         decoration:BoxDecoration(
           shape:BoxShape.circle,
           border:Border.all(color:AppColors.WHITE_COLOR,width:2.0)
         ),
         child:Center(child: Text(goalTime.toString(),style:TextStyle(color:AppColors.WHITE_COLOR)))
       );
  }

void completeDialog()
{
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
              borderRadius: BorderRadius.circular(1.0),
              border: Border(
                top: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                bottom: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                left: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),
                right: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.2)),


              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 15.0,),
                Text(AppStrings.HANG_ON_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),
                Padding(
                  padding: EdgeInsets.only(left:25.0,right: 25.0,top: 15.0),
                    child: Text(AppStrings.COMPLETED_GOAL_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400,height: 1.7),textScaleFactor: 1.15,textAlign: TextAlign.center,)
                ),

                SizedBox(height: 15.0,),

                Container(
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(width: 0.5, color: AppColors.WHITE_COLOR.withOpacity(0.3)),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: (){
                          AppNavigation.navigatorPop(context);
                          changeGoalStatus(setStatus:"completed");
                        },
                        child: Padding(
                          padding:EdgeInsets.only(top:13.0,bottom: 13.0),
                            child: Text(AppStrings.YES_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.2,textAlign: TextAlign.center,)
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: (){
                          AppNavigation.navigatorPop(context);
                        },
                        child: Container(
                          color: AppColors.LIGHT_GREY_COLOR.withOpacity(0.6),
                          child: Padding(
                              padding:EdgeInsets.only(top:13.0,bottom: 13.0),
                              child: Text(AppStrings.NO_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w600,height: 1.7),textScaleFactor: 1.2,textAlign: TextAlign.center,)
                          ),
                        ),
                      ),
                    ),
                    ],
                  ),
                )

              ],
            ),
          ),
        );
      });
}





void changeGoalStatus({String setStatus}) async
{
  token = await FirebaseMessagingService().getToken();
  AppDialogs.progressAlertDialog(context: context);
if(setStatus == "started")
  {

    goalList[goalIndex]["status"] = setStatus;
    goalList[goalIndex]["start_date"] = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());

    users.doc(token).collection("user_goals").doc(goalDateFormat)
        .set({"goals": goalList})
        .then((value) {
      AppNavigation.navigateCloseDialog(context);
      AppDialogs.showToast(message: AppStrings.GOAL_STARTED_TEXT);


      SharedPreference().setGoalList(goalList:jsonEncode({"goals": goalList}));
      SharedPreference().setGoalDate(date:goalDateFormat);

      AppNavigation.navigateReplacement(
          context, CurrentGoalScreen(setGoalList: goalList,setGoalIndex:goalIndex,setGoalDate:goalDateFormat));
    }).catchError((error){
      print("Failed to add user: $error");
      AppDialogs.showToast(message: error.message);
    });

  }

else if(setStatus == "completed")
  {
    goalList[goalIndex]["status"] = setStatus;
    goalList[goalIndex]["end_date"] = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());

    users.doc(token).collection("user_goals").doc(goalDateFormat)
        .set({"goals": goalList})
        .then((value) {
      AppNavigation.navigateCloseDialog(context);
      AppDialogs.showToast(message: AppStrings.GOAL_COMPLETED_TEXT);


      SharedPreference().setGoalList(goalList:jsonEncode({"goals": goalList}));
      SharedPreference().setGoalDate(date:goalDateFormat);


      if(goalIndex == 5)
        {
          SharedPreference().setCurrentGoalIndex(index:null);

          AppNavigation.navigateReplacement(
              context, DrawerScreen(menuItemNo: 1,));
        }

        else
          {

            goalIndex++;
            increaseGoalIndex = goalIndex;
            SharedPreference().setCurrentGoalIndex(index:increaseGoalIndex);

            AppNavigation.navigateReplacement(
                context, CurrentGoalScreen(setGoalList: goalList,setGoalIndex:increaseGoalIndex,setGoalDate:goalDateFormat));
          }

    }).catchError((error){
      print("Failed to add user: $error");
      AppDialogs.showToast(message: error.message);
    });
  }


}


void showTime()
{


  if(goalList[goalIndex]["status"] == "started")
  {
    goalTimer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      String currentDateTime = DateFormat('yyyy-MM-dd HH:mm:ss').format(
          DateTime.now());
      dateDifference = DateTime.parse(currentDateTime).difference(
          DateTime.parse(goalList[goalIndex]["start_date"]));
      goalTime = dateDifference.toString();
      setState(() {

      });
    });
  }

  else if(goalList[goalIndex]["status"] == "completed")
    {
      dateDifference = DateTime.parse(goalList[goalIndex]["end_date"]).difference(DateTime.parse(goalList[goalIndex]["start_date"]));



      goalTime = dateDifference.toString();
      setState(() {

      });

    }





}


  void getUserData()
  {
    if(SharedPreference().getUser() != null)
    {
      userName = jsonDecode(SharedPreference().getUser())["name"];
      profileNetworkImage = jsonDecode(SharedPreference().getUser())["profileImage"];


    }
  }




  @override
  void dispose() {
    // TODO: implement initState
    super.dispose();
    goalTimer != null ? goalTimer.cancel() : print("nothing");
  }

}