import 'package:flutter/material.dart';
import 'package:odaat_app/set_goal/screens/current_goal_screen.dart';
import 'package:odaat_app/support_us/screens/support_us_screen.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/widgets/custom_raised_button.dart';
import 'package:odaat_app/widgets/custom_text_form_field_first.dart';


class SetGoalScreen extends StatefulWidget {
  @override
  _SetGoalScreenState createState() => _SetGoalScreenState();
}

class _SetGoalScreenState extends State<SetGoalScreen> {

  final _setGoalForm = GlobalKey<FormState>();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _eMailController = TextEditingController();
  TextEditingController _sleepingTimeController = TextEditingController();
  TextEditingController _wakeUpController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(AssetPaths.SPLASH_BACKGROUND_IMAGE),
          fit: BoxFit.fill
        )
      ),
      child: Scaffold(
        backgroundColor: AppColors.TRANSPARENT_COLOR,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              heyWinnerAppBar(),
              Padding(
                padding: EdgeInsets.only(left: 15.0,right: 12.0,top:15.0),
                child:Text(AppStrings.BETTER_KNOW_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w100),textScaleFactor: 1.13,),
              ),
              Padding(
                padding: EdgeInsets.only(left: 15.0,right: 12.0,top:20.0),
                child:Text(AppStrings.UPDATE_LETTER_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w100),textScaleFactor: 1.13,),
              ),

              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.03),
                  margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.03),
                  decoration: BoxDecoration(
                    color: AppColors.PRIMARY_COLOR,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppColors.SHADOW_COLOR,
                          blurRadius: 4.0,
                          spreadRadius: 5.0,
                          offset: Offset(0.0, 0.0)
                      )
                    ],
                  ),
                  child: Form(
                    key: _setGoalForm,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                        labelText(AppStrings.YOUR_NAME_TEXT),
                        nameTextFormField(),
                        SizedBox(height: 20.0,),
                        labelText(AppStrings.YOUR_EMAIL_TEXT),
                        emailTextFormField(),
                        SizedBox(height: 20.0,),
                        labelText(AppStrings.TIME_TO_BED_TEXT),
                        goBedTextFormField(),
                        SizedBox(height: 20.0,),
                        labelText(AppStrings.WAKE_UP_FROM_BED_TEXT),
                        wakeUpTextFormField(),
                        Padding(
                          padding: EdgeInsets.only(left: 18.0,right:18.0,top:15.0),
                          child: RichText(
                            text: TextSpan(
                              text: AppStrings.MARK_QUOTE_TEXT,
                              style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w400,fontSize: 17.0,fontStyle: FontStyle.italic),
                              children: <TextSpan>[
                              TextSpan(text: AppStrings.MARK_TEXT, style: TextStyle(fontWeight: FontWeight.w800,fontStyle: FontStyle.normal)),

                              ],
                            ),
                          ),
                        ),

                        SizedBox(height: 15.0,),

                        Align(
                          alignment: Alignment.center,
                            child: nextButton()
                        ),
                        SizedBox(height: 5.0,),
                        ],
                      ),
                    ),
                  ),
                ),
              )


            ],
          ),
        ),
      ),
    );
  }


  Widget heyWinnerAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 15.0,right: 2.0,top:MediaQuery.of(context).size.height*0.05),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(AppStrings.HEY_WINNER_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),
          IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.arrow_forward_ios,color: AppColors.WHITE_COLOR,),
            onPressed: ()
            {
              AppNavigation.navigatorPop(context);
            },
          ),

        ],
      ),
    );
  }

  Widget labelText(String text)
  {
    return Padding(
        padding: EdgeInsets.only(left: 18.0,right:18.0),
        child: Text(text,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w600),textScaleFactor: 1.03,)
    );
  }

  Widget nameTextFormField()
  {
    return CustomTextFormFieldFirst(
     controller: _nameController,
     hintText: AppStrings.NAME_HINT,
     validator: (value){
        if(value.trim().isEmpty)
          {
            return AppStrings.NAME_ERROR;
          }
          return null;
     },
    );
  }

  Widget emailTextFormField()
  {
    return CustomTextFormFieldFirst(
      controller: _eMailController,
      hintText: AppStrings.EMAIL_HINT,
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.EMAIL_ERROR;
        }
        return null;
      },
    );
  }

  Widget goBedTextFormField()
  {
    return CustomTextFormFieldFirst(
      controller: _sleepingTimeController,
      hintText: AppStrings.SLEEPING_TIME_HINT,
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.SLEEPING_TIME_ERROR;
        }
        return null;
      },
    );
  }

  Widget wakeUpTextFormField()
  {
    return CustomTextFormFieldFirst(
      controller: _wakeUpController,
      hintText: AppStrings.WAKE_UP_TIME_HINT,
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.WAKE_UP_ERROR;
        }
        return null;
      },
    );
  }


 Widget nextButton()
 {
   return CustomRaisedButton(
     width:  MediaQuery.of(context).size.width*0.42,
     height: MediaQuery.of(context).size.height*0.062,
     text: AppStrings.NEXT_TEXT,
     onPressed: (){
     if(_setGoalForm.currentState.validate())
       {
         AppNavigation.navigateTo(context,  CurrentGoalScreen());
       }
       else
         {
           print("error");
         }
     },

   );
 }

}
