import 'package:flutter/material.dart';
import 'package:odaat_app/set_goal/screens/current_goal_screen.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_dialogs.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/widgets/custom_raised_button.dart';
import 'package:odaat_app/widgets/custom_text_form_field_first.dart';


class ShowGoalScreen extends StatefulWidget {
  List goalList;
  int index;
  String goalDate;
  bool nextScreen;
  ShowGoalScreen({this.goalList,this.index,this.goalDate,this.nextScreen});
  @override
  _ShowGoalScreenState createState() => _ShowGoalScreenState();
}

class _ShowGoalScreenState extends State<ShowGoalScreen> {
  TextEditingController _subjectController = TextEditingController();
  TextEditingController _messageController = TextEditingController();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print("goalList:${widget.goalList}");
    print("index:${widget.index}");
    print("goalDate:${widget.goalDate}");

  }




  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AssetPaths.SPLASH_BACKGROUND_IMAGE),
              fit: BoxFit.fill
          )
      ),
      child: Scaffold(
        backgroundColor: AppColors.TRANSPARENT_COLOR,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              showGoalAppBar(),

              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
                  margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
                  decoration: BoxDecoration(
                    color: AppColors.PRIMARY_COLOR,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppColors.SHADOW_COLOR,
                          blurRadius: 4.0,
                          spreadRadius: 5.0,
                          offset: Offset(0.0, 0.0)
                      )
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      labelText(widget.goalList[widget.index]["goalTitle"].toString()),
                      SizedBox(height:10.0),
                      Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.03,right: MediaQuery.of(context).size.width*0.03),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(left:10.0,right:10.0,top:10.0,bottom:10.0),
                              decoration: BoxDecoration(
                                color: AppColors.LIGHT_GREY_COLOR,
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: SingleChildScrollView(
                                child: Text(
                                  widget.goalList[widget.index]["goalMessage"],
                                 style: TextStyle(color:AppColors.WHITE_COLOR,height: 1.5),
                                  textScaleFactor: 1.25,
                                ),
                              ),

                            ),
                          )
                      ),

                      SizedBox(height: 5.0,),
                      Align(
                          alignment: Alignment.center,
                          child: widget.nextScreen == true ? Container() : nextButton()
                      ),
                      SizedBox(height: 5.0,),
                    ],
                  ),
                ),
              )


            ],
          ),
        ),
      ),
    );
  }

  Widget showGoalAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 15.0,right: 2.0,top:MediaQuery.of(context).size.height*0.05),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(AppStrings.GOAL_SHOW_TEXT+" ${widget.index + 1}" ,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),
          IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.arrow_forward_ios,color: AppColors.WHITE_COLOR,),
            onPressed: ()
            {
              AppNavigation.navigatorPop(context);
            },
          ),

        ],
      ),
    );
  }

  Widget labelText(String text)
  {
    return Padding(
        padding: EdgeInsets.only(left: 18.0,right:18.0,bottom: 5.0),
        child: Text(text,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w600),textScaleFactor: 2.0,maxLines: 4,overflow: TextOverflow.ellipsis,)
    );
  }




  Widget nextButton()
  {
    return CustomRaisedButton(
        width:  MediaQuery.of(context).size.width*0.72,
        height: MediaQuery.of(context).size.height*0.062,
        text: AppStrings.SHOW_GOAL_STATUS_TEXT,
        onPressed: () {
goToCurrentGoalScreen();

        }

    );
  }


void goToCurrentGoalScreen()
{

  if(widget.index == 0)
    {
      AppNavigation.navigateReplacement(
          context, CurrentGoalScreen(setGoalList: widget.goalList,setGoalIndex:widget.index,setGoalDate:widget.goalDate));
    }
  else if(widget.goalList[widget.index - 1]["status"] == "completed")
  {
    AppNavigation.navigateReplacement(
        context, CurrentGoalScreen(setGoalList: widget.goalList,setGoalIndex:widget.index,setGoalDate:widget.goalDate));
  }
  else
    {
      AppDialogs.showToast(message:"Please complete above goal");
    }

}
}
