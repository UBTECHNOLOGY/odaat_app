import 'package:flutter/material.dart';
import 'package:odaat_app/set_goal/screens/current_goal_screen.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_dialogs.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/widgets/custom_raised_button.dart';
import 'package:flutter/services.dart';


class AddGoalScreen extends StatefulWidget {
  @override
  _AddGoalScreenState createState() => _AddGoalScreenState();
}

class _AddGoalScreenState extends State<AddGoalScreen> {
  TextEditingController _goalTitle = TextEditingController();
  TextEditingController _goalMessage = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AssetPaths.SPLASH_BACKGROUND_IMAGE),
              fit: BoxFit.fill
          )
      ),
      child: Scaffold(
        backgroundColor: AppColors.TRANSPARENT_COLOR,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              showGoalAppBar(),

              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
                  margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
                  decoration: BoxDecoration(
                    color: AppColors.PRIMARY_COLOR,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppColors.SHADOW_COLOR,
                          blurRadius: 4.0,
                          spreadRadius: 5.0,
                          offset: Offset(0.0, 0.0)
                      )
                    ],
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        labelText(AppStrings.GOAL_SHOW_TEXT),
                        SizedBox(height:10.0),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.03,right: MediaQuery.of(context).size.width*0.03),
                          child: TextField(
                            controller: _goalTitle,
                            decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(color: AppColors.LIGHT_GREY_COLOR)
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(color: AppColors.LIGHT_GREY_COLOR)
                                ),
                                fillColor: AppColors.LIGHT_GREY_COLOR,
                                filled: true,
                                hintText: AppStrings.GOAL_TITLE_HINT,
                                hintStyle: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.7)),
                            ),
                            maxLines: 1,
                            style: TextStyle(color: AppColors.WHITE_COLOR),
//                            inputFormatters: [
//                              LengthLimitingTextInputFormatter(1),
//                            ],

                          ),
                        ),
                        SizedBox(height:10.0),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.03,right: MediaQuery.of(context).size.width*0.03),
                          child: TextField(
                            controller: _goalMessage,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: AppColors.LIGHT_GREY_COLOR)
                              ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(color: AppColors.LIGHT_GREY_COLOR)
                                ),
                              fillColor: AppColors.LIGHT_GREY_COLOR,
                              filled: true,
                              hintText: AppStrings.GOAL_Message_HINT,
                              hintStyle: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.7))
                            ),
                            maxLines: 12,
                            style: TextStyle(color: AppColors.WHITE_COLOR),
                          ),
                        ),

                        SizedBox(height: 20.0,),
                        Align(
                            alignment: Alignment.center,
                            child: nextButton()
                        ),
                        SizedBox(height: 5.0,),
                      ],
                    ),
                  ),
                ),
              )


            ],
          ),
        ),
      ),
    );
  }

  Widget showGoalAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 15.0,right: 2.0,top:MediaQuery.of(context).size.height*0.05),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(AppStrings.ADD_GOAL_TEXT ,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),
          IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.arrow_forward_ios,color: AppColors.WHITE_COLOR,),
            onPressed: ()
            {
              AppNavigation.navigatorPop(context);
            },
          ),

        ],
      ),
    );
  }

  Widget labelText(String text)
  {
    return Padding(
        padding: EdgeInsets.only(left: 18.0,right:18.0,bottom: 5.0),
        child: Text(text,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w600),textScaleFactor: 2.0,)
    );
  }




  Widget nextButton()
  {
    return CustomRaisedButton(
        width:  MediaQuery.of(context).size.width*0.5,
        height: MediaQuery.of(context).size.height*0.062,
        text: AppStrings.ADD_TEXT,
        onPressed: () {

          if(_goalTitle.text.trim().isNotEmpty)
            {
              AppNavigation.navigatorPopData(context,
                  {
                    "goalTitle": _goalTitle.text,
                    "goalMessage": _goalMessage.text,
                    "status":"pending",
                    "start_date":"",
                    "end_date":"",
                    "total_time":"0"
                  }
              );
            }
            else
              {
                AppDialogs.showToast(message: AppStrings.GOALS_EMPTY_ERROR);
              }





        }

    );
  }
}
