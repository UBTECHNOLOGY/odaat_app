import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:odaat_app/drawer/screens/drawer_screen.dart';
import 'package:odaat_app/services/firebase_messaging_service.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_dialogs.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/widgets/custom_raised_button.dart';
import 'package:odaat_app/widgets/custom_text_form_field_first.dart';
import 'package:odaat_app/services/shared_preference.dart';


class SuggestionScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  SuggestionScreen({this.scaffoldKey});
  @override
  _SuggestionScreenState createState() => _SuggestionScreenState();
}

class _SuggestionScreenState extends State<SuggestionScreen> {
  TextEditingController _subjectController = TextEditingController();
  TextEditingController _messageController = TextEditingController();
  final _suggestionKey = GlobalKey<FormState>();
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  String token;
  Map<String,dynamic> userSuggestion = Map<String,dynamic>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserSuggestion();
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          suggestionAppBar(),
          Padding(
            padding: EdgeInsets.only(left: 15.0,right: 12.0,top:15.0),
            child:Text(AppStrings.PROVIDE_SUGGESTION_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w100),textScaleFactor: 1.13,),
          ),

          Expanded(
            child: Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.05),
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.05),
              decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: AppColors.SHADOW_COLOR,
                      blurRadius: 4.0,
                      spreadRadius: 5.0,
                      offset: Offset(0.0, 0.0)
                  )
                ],
              ),
              child: Form(
                key:_suggestionKey,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      labelText(AppStrings.SUBJECT_TEXT),
                      subjectTextFormField(),
                      SizedBox(height: 25.0,),
                      labelText(AppStrings.MESSAGE_TEXT),
                      messageTextFormField(),
                      SizedBox(height: 25.0,),


                      Align(
                          alignment: Alignment.center,
                          child: nextButton()
                      ),
                      SizedBox(height: 5.0,),
                    ],
                  ),
                ),
              ),
            ),
          )


        ],
      ),
    );
  }

  Widget suggestionAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 5.0,right:10.0,top:MediaQuery.of(context).size.height*0.03),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Spacer(),

          SizedBox(width: 10.0,),

          Text(AppStrings.SUGGESTION_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.9,),



          Spacer(),

          GestureDetector(
            onTap: (){
              //AppNavigation.navigateTo(context, HowItWorks());
              widget.scaffoldKey.currentState.openDrawer();
            },
            child: Image.asset(AssetPaths.MORE_ICON,width: 28.0,),
          )
        ],
      ),
    );
  }

  Widget labelText(String text)
  {
    return Padding(
        padding: EdgeInsets.only(left: 18.0,right:18.0,bottom: 5.0),
        child: Text(text,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w600),textScaleFactor: 1.03,)
    );
  }

  Widget subjectTextFormField()
  {
    return CustomTextFormFieldFirst(
      controller: _subjectController,
      hintText: AppStrings.SUBJECT_HINT,
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.NAME_ERROR;
        }
        return null;
      },
    );
  }

  Widget messageTextFormField()
  {
    return CustomTextFormFieldFirst(
      controller: _messageController,
      hintText: AppStrings.MESSAGE_HINT,
      maxLines: 5,
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.EMAIL_ERROR;
        }
        return null;
      },
    );
  }


  Widget nextButton()
  {
    return CustomRaisedButton(
      width:  MediaQuery.of(context).size.width*0.42,
      height: MediaQuery.of(context).size.height*0.062,
      text: AppStrings.SAVE_TEXT,
      onPressed: () {
        if(_suggestionKey.currentState.validate())
        {
          //AppNavigation.navigateTo(context,  SupportUsScreen());
          userSuggestion = {"suggestionSubject":_subjectController.text,"suggestionMessage":_messageController.text};
          sentSuggestionData();
        }
        else
        {
          print("error");
        }
      },


    );
  }


  sentSuggestionData() async {

    AppDialogs.progressAlertDialog(context: context);
    token = await FirebaseMessagingService().getToken();

    users.doc(token).
    set(userSuggestion)
        .then((value) {
      //goalsList.clear();
     SharedPreference().setSuggestion(suggestion:jsonEncode(userSuggestion));
      AppNavigation.navigateCloseDialog(context);
      AppDialogs.showToast(message: "Suggestion Added");
     AppNavigation.navigateToRemovingAll(context,  DrawerScreen(menuItemNo: 1,));


    })
        .catchError((error){
      print("Failed to add user: $error");
      AppNavigation.navigateCloseDialog(context);
      AppDialogs.showToast(message: "Suggestion can not be added");
    });

  }


  void getUserSuggestion()
  {
    if(SharedPreference().getSuggestion() != null)
    {
      userSuggestion = jsonDecode(SharedPreference().getSuggestion());

      _subjectController.text =  userSuggestion["suggestionSubject"];
      _messageController.text =  userSuggestion["suggestionMessage"];


    }
  }
}
