import 'package:flutter/material.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/utils/app_strings.dart';
class AboutUsScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  AboutUsScreen({this.scaffoldKey});
  @override
  _AboutUsScreenState createState() => _AboutUsScreenState();
}

class _AboutUsScreenState extends State<AboutUsScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          aboutUsAppBar(),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
              decoration: BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: AppColors.SHADOW_COLOR,
                      blurRadius: 4.0,
                      spreadRadius: 5.0,
                      offset: Offset(0.0, 0.0)
                  )
                ],
              ),
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(left: 20.0,right: 20.0,bottom: 10.0),
                  child: Column(
                    children: [
                      Text(AppStrings.ABOUT_US_1_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w500,height: 1.7),textScaleFactor: 1.2,),

                      SizedBox(height: 20.0,),

                      Text(AppStrings.ABOUT_US_2_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w500,height: 1.7),textScaleFactor: 1.2,),

                      SizedBox(height:20.0),

                      Text(AppStrings.ABOUT_US_3_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w500,height: 1.7),textScaleFactor: 1.2,),


                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }


  Widget aboutUsAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 15.0,right:10.0,top:MediaQuery.of(context).size.height*0.03),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Spacer(),

          SizedBox(width: 10.0,),

          Container(
            width: MediaQuery.of(context).size.width*0.5,
            height: MediaQuery.of(context).size.height*0.05,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(AssetPaths.ABOUT_US_IMAGE),
                fit: BoxFit.cover
              )
            ),
          ),
          

          Spacer(
            flex: 2,
          ),
          GestureDetector(
            onTap: (){
              //AppNavigation.navigateTo(context, HowItWorks());
              widget.scaffoldKey.currentState.openDrawer();
            },
            child: Image.asset(AssetPaths.MORE_ICON,width: 28.0,),
          )
        ],
      ),
    );
  }
}
