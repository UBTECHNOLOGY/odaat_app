import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:odaat_app/about_us/about_us_screen.dart';
import 'package:odaat_app/edit_profile/screens/edit_profile_screen.dart';
import 'package:odaat_app/home/screens/home_screen.dart';
import 'package:odaat_app/services/shared_preference.dart';
import 'package:odaat_app/suggestion/suggestion_screen.dart';
import 'package:odaat_app/support_us/screens/support_us_screen.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/how_it_works/how_it_works_screen.dart';


class DrawerScreen extends StatefulWidget {
  int menuItemNo;
  DrawerScreen({this.menuItemNo});

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  Widget drawerShowWidget;
  String userName = AppStrings.USER_NAME_MENU_TEXT;
  String profileNetworkImage = null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("new");

    getUserData();

    drawerShowWidget = widget.menuItemNo == 1 ? HomeScreen(scaffoldKey: scaffoldKey) : HowItWorks(scaffoldKey: scaffoldKey);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AssetPaths.SPLASH_BACKGROUND_IMAGE),
              fit: BoxFit.fill
          )
      ),
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: AppColors.TRANSPARENT_COLOR,
      drawer: Drawer(
        child: Container(
          color: AppColors.PRIMARY_COLOR,
          child: SafeArea(
            child: Column(
              children: [
               SizedBox(
                 height: MediaQuery.of(context).size.height*0.05,
               ),
               userProfile(),

               SizedBox(height: 15.0,),

               Expanded(
                 child: Container(
                   width: MediaQuery.of(context).size.width,
                   padding: EdgeInsets.only(top: 20.0),
                   decoration: BoxDecoration(
                   ),
                   child: ListView(
                     padding: EdgeInsets.zero,
                     children: [

                       Container(
                         width: MediaQuery.of(context).size.width,
                         height: 5.0,
                         color: AppColors.SHADOW_COLOR,
                       ),

                       SizedBox(height: 30.0,),

                       menuData(AssetPaths.HOME_MENU_ICON, AppStrings.HOME_TEXT,1),
                       menuData(AssetPaths.ABOUT_US_MENU_ICON, AppStrings.ABOUT_US_TEXT,2),
                       menuData(AssetPaths.SUPPORT_US_MENU_ICON, AppStrings.SUPPORT_US_TEXT,3),
                       menuData(AssetPaths.EDIT_PROFILE_MENU_ICON, AppStrings.EDIT_PROFILE_TEXT,4),
                       menuData(AssetPaths.ABOUT_US_MENU_ICON, AppStrings.SUGGESTION_TEXT,5),
                       menuData(AssetPaths.SUPPORT_US_MENU_ICON, AppStrings.HOW_IT_WORKS_TEXT,6),
                     ],
                   ),
                 ),
               )


              ],
            ),
          ),
        ),
      ),
        body:  drawerShowWidget,
      ),
    );
  }

  Widget userProfile()
  {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                color: AppColors.WHITE_COLOR,
                onPressed: (){
                  AppNavigation.navigatorPop(context);
                },
              ),
            ),

            Spacer(
              flex: 1,
            ),

            Container(
              width: 105.0,
              height: 105.0,
              margin: EdgeInsets.only(top: 10.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border:Border.all(color: AppColors.WHITE_COLOR,width: 2.0),
                image: DecorationImage(
                  image: profileNetworkImage != null ?
                  NetworkImage(profileNetworkImage)
                      :

                  AssetImage(AssetPaths.USER_PROFILE_MENU_IMAGE),
                  fit: BoxFit.fill
                )
              ),
            ),

            Spacer(
              flex: 2,
            ),
          ],
        ),

        SizedBox(height: 10.0,),

        Text(AppStrings.GOOD_MORNING_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w400),textScaleFactor: 1.2,),

        SizedBox(height: 8.0,),

        Text(userName,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w700),textScaleFactor: 1.2,),

      ],
    );
  }


  Widget menuData(String iconPath,String title,int index)
  {
    return ListTile(
      leading: Image.asset(iconPath,width: 23.0,),
      title: Text(title,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.w500),textScaleFactor: 1.3,),
      trailing: Padding(
          padding: EdgeInsets.only(top: 2.0),
          child: Image.asset(AssetPaths.NEXT_MENU_ICON,width: 15.0,)
      ),
      dense: true,
      onTap: (){
        moveToNewScreen(index);
      },
    );
  }


  void moveToNewScreen(int index)
  {
     if(index == 1)
       {
         AppNavigation.navigatorPop(context);
         drawerShowWidget = HomeScreen(scaffoldKey: scaffoldKey);
       }
     else if(index == 2)
     {
       AppNavigation.navigatorPop(context);
       drawerShowWidget = AboutUsScreen(scaffoldKey: scaffoldKey);
     }
     else if(index == 3)
     {
       AppNavigation.navigatorPop(context);
       drawerShowWidget = SupportUsScreen(scaffoldKey: scaffoldKey);
     }
     else if(index == 4)
     {
         AppNavigation.navigatorPop(context);
         drawerShowWidget = EditProfileScreen(scaffoldKey: scaffoldKey);
     }

     else if(index == 5)
     {
       AppNavigation.navigatorPop(context);
       drawerShowWidget = SuggestionScreen(scaffoldKey: scaffoldKey);
     }

     else if(index == 6)
     {
       AppNavigation.navigatorPop(context);
       drawerShowWidget = HowItWorks(scaffoldKey: scaffoldKey);
     }

     setState(() {

     });

  }


  void getUserData()
  {
    if(SharedPreference().getUser() != null)
    {
      userName = jsonDecode(SharedPreference().getUser())["name"];
      profileNetworkImage = jsonDecode(SharedPreference().getUser())["profileImage"];


    }
  }



}
