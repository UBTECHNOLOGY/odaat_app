import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:odaat_app/drawer/screens/drawer_screen.dart';
import 'package:odaat_app/services/app_notification.dart';
import 'package:odaat_app/services/firebase_messaging_service.dart';
import 'package:odaat_app/services/shared_preference.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_dialogs.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/widgets/custom_raised_button.dart';
import 'package:odaat_app/widgets/custom_text_form_field_second.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as bt;


class EditProfileScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  EditProfileScreen({this.scaffoldKey});
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  final _setGoalForm = GlobalKey<FormState>();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _eMailController = TextEditingController();
  TextEditingController _sleepingTimeController = TextEditingController();
  TextEditingController _wakeUpController = TextEditingController();

  TimeOfDay sleepingTimePickedTime;
  TimeOfDay wakeUpPickedTime;

  String sleepingTimePickedTimeFormat;
  String wakeUpPickedTimeFormat;
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  Map<String,dynamic> userProfile = Map<String,dynamic>();

  String token;

  AppNotification appNotification = AppNotification();

  File profileFileImage;
  ImagePicker picker = ImagePicker();
  PickedFile pickedFile;
  String fileName;
  String downloadImageUrl= null;
  String profileNetworkImage;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserProfile();
    appNotification.initializeNotification();
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.scaffoldKey == null ?
          (){
        AppNavigation.navigateReplacement(context,  DrawerScreen(menuItemNo: 1,));
      }
          :
      null,
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AssetPaths.SPLASH_BACKGROUND_IMAGE),
                fit: BoxFit.fill
            )
        ),
        child: Scaffold(
          backgroundColor: AppColors.TRANSPARENT_COLOR,
          body: SafeArea(
            child: Column(
              children: [
                editProfileAppBar(),

                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.035),
                    margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.06),
                    decoration: BoxDecoration(
                      color: AppColors.PRIMARY_COLOR,
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(22.0),topRight:Radius.circular(22.0)),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: AppColors.SHADOW_COLOR,
                            blurRadius: 4.0,
                            spreadRadius: 5.0,
                            offset: Offset(0.0, 0.0)
                        )
                      ],
                    ),
                    child: Form(
                      key: _setGoalForm,
                      child: SingleChildScrollView(
                        child: Column(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            labelText(AppStrings.YOUR_NAME_TEXT),
                            nameTextFormField(),
                            SizedBox(height: 25.0,),
                            labelText(AppStrings.YOUR_EMAIL_TEXT),
                            emailTextFormField(),
                            SizedBox(height: 25.0,),
                            labelText(AppStrings.TIME_TO_BED_TEXT),
                            goBedTextFormField(),
                            SizedBox(height: 25.0,),
                            labelText(AppStrings.WAKE_UP_FROM_BED_TEXT),
                            wakeUpTextFormField(),
//                          Padding(
//                            padding: EdgeInsets.only(left: 18.0,right:18.0,top:15.0),
//                            child: RichText(
//                              text: TextSpan(
//                                text: AppStrings.MARK_QUOTE_TEXT,
//                                style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w400,fontSize: 17.0,fontStyle: FontStyle.italic),
//                                children: <TextSpan>[
//                                  TextSpan(text: AppStrings.MARK_TEXT, style: TextStyle(fontWeight: FontWeight.w800,fontStyle: FontStyle.normal)),
//
//                                ],
//                              ),
//                            ),
//                          ),

                            SizedBox(height: 35.0,),

                            Align(
                              alignment: Alignment.center,
                              child: nextButton(),
                            ),
                            SizedBox(height: 25.0,),

                            Text(AppStrings.LATER_CHANGE_SETTING_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w500),textScaleFactor: 1.0,textAlign: TextAlign.center,),

                            SizedBox(height: 15.0,),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget editProfileAppBar()
  {
    return Padding(
      padding: EdgeInsets.only(left: 15.0,right:10.0,top:MediaQuery.of(context).size.height*0.03),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Container(
            width: 80.0,
            height: 80.0,
            child: Stack(
              children: [
                Container(
                  width: 80.0,
                  height: 80.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border:Border.all(color: AppColors.WHITE_COLOR,width: 2.0),
                    image: DecorationImage(
                      image:
                      profileFileImage !=null ?
                      FileImage(profileFileImage)
                      :
                      profileNetworkImage != null ?
                      NetworkImage(profileNetworkImage)
                      :
                      AssetImage(AssetPaths.USER_PROFILE_MENU_IMAGE),
                      fit: BoxFit.fill
                    )
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: GestureDetector(
                    onTap: (){
                      imageGalleryOption();
                    },
                      child: Image.asset(AssetPaths.CAMERA_ICON,width: 25.0,)
                  ),
                ),
              ],
            ),
          ),

         Expanded(
           child: Padding(
             padding: EdgeInsets.only(top:17.0,left: 5.0,right: 5.0),
             child: Text(_nameController.text.isNotEmpty ? _nameController.text : AppStrings.USER_NAME_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 1.5,textAlign: TextAlign.center,),
           ),
         ),

          widget.scaffoldKey == null ?
          IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.arrow_forward_ios,color: AppColors.WHITE_COLOR,),
            onPressed: ()
            {
              AppNavigation.navigateReplacement(context,  DrawerScreen(menuItemNo: 1,));
            },
          )
              :
          GestureDetector(
            onTap: (){
              //AppNavigation.navigateTo(context, HowItWorks());
              widget.scaffoldKey.currentState.openDrawer();
            },
            child: Image.asset(AssetPaths.MORE_ICON,width: 28.0,),
          )
        ],
      ),
    );
  }


  Widget labelText(String text)
  {
    return Text(text,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.8),fontWeight: FontWeight.w500),textScaleFactor: 1.1,);
  }


  Widget nameTextFormField()
  {
    return CustomTextFormFieldSecond(
      controller: _nameController,
      hintText: AppStrings.NAME_HINT,
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.NAME_ERROR;
        }
        return null;
      },
    );
  }


  Widget emailTextFormField()
  {
    return CustomTextFormFieldSecond(
      controller: _eMailController,
      hintText: AppStrings.EMAIL_HINT,
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.EMAIL_ERROR;
        }
        return null;
      },
    );
  }

  Widget goBedTextFormField()
  {
    return CustomTextFormFieldSecond(
      controller: _sleepingTimeController,
      hintText: AppStrings.SLEEPING_TIME_HINT,
      readOnly: true,
      onTap: (){
        goToBedTime();
      },
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.SLEEPING_TIME_ERROR;
        }
        return null;
      },
    );
  }

  Widget wakeUpTextFormField()
  {
    return CustomTextFormFieldSecond(
      controller: _wakeUpController,
      hintText: AppStrings.WAKE_UP_TIME_HINT,
      readOnly: true,
      onTap: (){
        wakeUpTime();
      },
      validator: (value){
        if(value.trim().isEmpty)
        {
          return AppStrings.WAKE_UP_ERROR;
        }
        return null;
      },
    );
  }


  Widget nextButton()
  {
    return CustomRaisedButton(
      width:  MediaQuery.of(context).size.width*0.42,
      height: MediaQuery.of(context).size.height*0.062,
      text: AppStrings.SAVE_TEXT,
      onPressed: (){

        saveProfile();
//        if(_setGoalForm.currentState.validate())
//        {
//          //AppNavigation.navigateTo(context,  SupportUsScreen());
//        }
//        else
//        {
//          print("error");
//        }
      },

    );
  }



  void goToBedTime() async
  {
    sleepingTimePickedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (sleepingTimePickedTime!= null)
    {
      sleepingTimePickedTimeFormat = DateFormat('HH:mm').format(DateTime(2021,08,12,sleepingTimePickedTime.hour,sleepingTimePickedTime.minute));

      _sleepingTimeController.text = sleepingTimePickedTimeFormat;


    }


  }


  void wakeUpTime() async
  {
    wakeUpPickedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (wakeUpPickedTime!= null)
    {
      wakeUpPickedTimeFormat = DateFormat('HH:mm').format(DateTime(2021,08,12,wakeUpPickedTime.hour,wakeUpPickedTime.minute));

      _wakeUpController.text = wakeUpPickedTimeFormat;
    }
  }





  void saveProfile() async
  {

    if(_nameController.text.trim().isEmpty || _eMailController.text.trim().isEmpty || _sleepingTimeController.text.trim().isEmpty || _wakeUpController.text.trim().isEmpty)
    {
      AppDialogs.showToast(message: AppStrings.EDIT_PROFILE_TOAST_TEXT);
      return;
    }

    AppDialogs.progressAlertDialog(context: context);
    token = await FirebaseMessagingService().getToken();



   if(profileNetworkImage != null)
     {
       userProfile = {"name":"${_nameController.text}","email":"${_eMailController.text}","sleepingTime":"${_sleepingTimeController.text}","wakeUpTime":"${_wakeUpController.text}","profileImage":profileNetworkImage};
     }

    else if(profileFileImage != null) {
      try {
        fileName = bt.basename(profileFileImage.path);
        await FirebaseStorage.instance
            .ref()
            .child(token)
            .child(fileName)
            .putFile(profileFileImage);

        downloadImageUrl = await FirebaseStorage.instance
            .ref()
            .child(token)
            .child(fileName)
            .getDownloadURL();

        print("Download url:${downloadImageUrl}");
        userProfile = {"name":"${_nameController.text}","email":"${_eMailController.text}","sleepingTime":"${_sleepingTimeController.text}","wakeUpTime":"${_wakeUpController.text}","profileImage":downloadImageUrl};
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
        downloadImageUrl = null;
        AppNavigation.navigateCloseDialog(context);
        print("exception ha image:${e}");
        AppDialogs.showToast(message: "error in image upload");
        return;
      }
    }

    else
      {
        userProfile = {"name":"${_nameController.text}","email":"${_eMailController.text}","sleepingTime":"${_sleepingTimeController.text}","wakeUpTime":"${_wakeUpController.text}","profileImage":null};
      }







    print(userProfile.toString());

    users.doc(token).set(userProfile)
        .then((value) {
      //goalsList.clear();
      SharedPreference().setUser(user:jsonEncode(userProfile));
      AppNavigation.navigateCloseDialog(context);
      AppDialogs.showToast(message: "Profile Updated");
      appNotification.scheduleGoalNotification();
      AppNavigation.navigateToRemovingAll(context,  DrawerScreen(menuItemNo: 1,));

        })
        .catchError((error){
      print("Failed to add user: $error");
      AppNavigation.navigateCloseDialog(context);
      AppDialogs.showToast(message: "Profile can not be updated");
    });
  }



  void getUserProfile()
  {
    if(SharedPreference().getUser() != null)
      {
        userProfile = jsonDecode(SharedPreference().getUser());

        _nameController.text = userProfile["name"];
        _eMailController.text = userProfile["email"];
        _sleepingTimeController.text = userProfile["sleepingTime"];
        _wakeUpController.text = userProfile["wakeUpTime"];
        profileNetworkImage = userProfile["profileImage"] != null ? userProfile["profileImage"] : null;


        //print("profileNetworkImage"+userProfile["profileImage"].toString());


      }
  }


  //Select Image Start
  void imageGalleryOption() {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            color: AppColors.WHITE_COLOR,
            child: SafeArea(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                    leading: Icon(
                      Icons.camera_enhance,
                      color: AppColors.PRIMARY_COLOR,
                    ),
                    title: Text(
                      AppStrings.CAMERA_TEXT,
                      style:
                      TextStyle(
                          fontWeight: FontWeight.bold, color: AppColors.PRIMARY_COLOR),
                    ),
                    onTap: () async {
                      getCameraImage();
                    },
                  ),
                  Divider(
                    color: AppColors.PRIMARY_COLOR,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.image,
                      color: AppColors.PRIMARY_COLOR,
                    ),
                    title: Text(
                      AppStrings.GALLERY_TEXT,
                      style:
                      TextStyle(
                          fontWeight: FontWeight.bold, color: AppColors.PRIMARY_COLOR),
                    ),
                    onTap: () async {
                      getGalleryImage();
                    },
                  )
                ],
              ),
            ),
          );
        }
    );
  }


  void getCameraImage() async
  {
    pickedFile = await picker.getImage(source: ImageSource.camera);
    if(pickedFile != null)
    {
      profileFileImage = File(pickedFile.path);
      profileNetworkImage = null;
    }
    else
      {
        profileFileImage = null;
      }
    AppNavigation.navigatorPop(context);
    setState(() {

    });
  }

  void getGalleryImage() async
  {
    pickedFile = await picker.getImage(source: ImageSource.gallery);
    if(pickedFile != null)
    {
      profileFileImage = File(pickedFile.path);
      profileNetworkImage = null;
    }
    else
    {
      profileFileImage = null;
    }
    AppNavigation.navigatorPop(context);

    setState(() {

    });

  }

}
