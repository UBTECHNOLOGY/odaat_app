import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:odaat_app/drawer/screens/drawer_screen.dart';
import 'package:odaat_app/how_it_works/how_it_works_screen.dart';
import 'package:odaat_app/services/firebase_messaging_service.dart';
import 'package:odaat_app/services/shared_preference.dart';
import 'package:odaat_app/set_goal/screens/current_goal_screen.dart';
import 'package:odaat_app/set_goal/screens/set_goal_screen.dart';
import 'package:odaat_app/support_us/screens/support_us_screen.dart';
import 'package:odaat_app/utils/app_colors.dart';
import 'package:odaat_app/utils/app_strings.dart';
import 'package:odaat_app/utils/asset_paths.dart';
import 'package:odaat_app/utils/navigation.dart';
import 'package:odaat_app/widgets/custom_raised_button.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

    //_splashTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AssetPaths.SPLASH_BACKGROUND_IMAGE),
            fit: BoxFit.fill
          )
        ),
        child: Column(
          children: [
            Spacer(),
            Container(
              width: MediaQuery.of(context).size.width*0.5,
              height: MediaQuery.of(context).size.height*0.25,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(AssetPaths.SPLASH_FOREGROUND_IMAGE),
                      fit: BoxFit.contain
                  )
              ),
            ),
            SizedBox(height:5.0),
            Text(AppStrings.ODAAT_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR,fontWeight: FontWeight.bold),textScaleFactor: 2.5,),
            Spacer(),
            Text(AppStrings.ONE_DAY_AT_TIME_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w100),textScaleFactor: 1.15,),
            SizedBox(height:18.0),
            Text(AppStrings.ONE_DAY_AT_GOAL_TEXT,style: TextStyle(color: AppColors.WHITE_COLOR.withOpacity(0.9),fontWeight: FontWeight.w100),textScaleFactor: 1.15,),
            SizedBox(height:MediaQuery.of(context).size.height*0.12),
            _tapToContinueWidget(),
            SizedBox(height:MediaQuery.of(context).size.height*0.1),

          ],
        ),
      ),
    );
  }


  Widget _tapToContinueWidget()
  {
    return CustomRaisedButton(
        width:  MediaQuery.of(context).size.width*0.7,
        height: MediaQuery.of(context).size.height*0.062,
        text: AppStrings.TAP_CONTINUE_TEXT,
        onPressed: () {
          onComplete();

        }

    );
  }


//  Future<Timer> _splashTimer() async {
//    return Timer(Duration(seconds: 3), onComplete);
//  }

  void onComplete() async {
    await SharedPreference().sharedPreference;
    String token = await FirebaseMessagingService().getToken();
    print("Token is"+token);

    if(SharedPreference().getInitialAppInstall() == null)
    {
      SharedPreference().setInitialAppInstall(initialOpen: true);
      AppNavigation.navigateReplacement(context,  HowItWorks());
      }
      else
        {
          if(SharedPreference().getCurrentGoalIndex() == null) {
            AppNavigation.navigateReplacement(
                context, DrawerScreen(menuItemNo: 1,));
          }
          else
            {
              AppNavigation.navigateReplacement(
                  context, CurrentGoalScreen(setGoalList: jsonDecode(SharedPreference().getGoalList())["goals"],setGoalIndex:SharedPreference().getCurrentGoalIndex(),setGoalDate:SharedPreference().getGoalDate()));
            }
        }


  }


}
