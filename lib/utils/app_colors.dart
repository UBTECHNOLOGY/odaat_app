import 'package:flutter/material.dart';

class AppColors
{
  static const Color PRIMARY_COLOR = Color(0XFF000000);
  static const Color WHITE_COLOR = Color(0XFFFFFFFF);
  static const Color LIGHT_GREY_COLOR = Color(0XFF474747);
  static const Color YELLOW_COLOR = Color(0XFFFFB61D);
  static const Color SHADOW_COLOR = Color(0XFF1D1C1C);
  static const Color DARK_COLOR = Color(0XFF0A0A0A);
  static const Color TRANSPARENT_COLOR = Colors.transparent;
  static Color RED_COLOR = Colors.red[800];
}