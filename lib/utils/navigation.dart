import 'package:flutter/material.dart';

class AppNavigation
{
  static Future<void> navigateToRemovingAll(context, Widget widget) async {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => widget),
            (Route<dynamic> route) => false);
  }

  static Future navigateTo(BuildContext context, Widget widget) async {
    return await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => widget,
      ),
    );
  }

  static Future<void> navigateReplacement(
      BuildContext context, Widget widget) async {
    return await Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => widget,
      ),
    );
  }

  static void navigatorPop(BuildContext context) {
    return Navigator.pop(context);
  }

  static void navigateCloseDialog(BuildContext context) async {
    Navigator.of(context, rootNavigator: true).pop();
  }

  static void navigatorPopTrue(BuildContext context) {
    return Navigator.of(context).pop(true);
  }

  static void navigatorPopData(BuildContext context, dynamic data) {
     Navigator.of(context).pop(data);
  }

  static void navigatorPopFalse(BuildContext context) {
    return Navigator.of(context).pop(false);
  }

}