class AppStrings
{
  // ----------------------- Text Messages --------------------- //
  static const String APP_TITLE_TEXT ="O'daat";
  static const String ODAAT_TEXT ="O'DAAT";
  static const String ONE_DAY_AT_TIME_TEXT ="ONE DAY AT A TIME";
  static const String ONE_DAY_AT_GOAL_TEXT ="ONE GOAL AT A TIME";
  static const String HEY_WINNER_TEXT ="Hey Winner!";
  static const String BETTER_KNOW_TEXT ="Answer these questions so we can know you better.";
  static const String UPDATE_LETTER_TEXT ="You can always update this later.";
  static const String YOUR_NAME_TEXT ="What is your name?";
  static const String YOUR_EMAIL_TEXT ="What is your e-mail?";
  static const String TIME_TO_BED_TEXT ="What time do you go to bed?";
  static const String WAKE_UP_FROM_BED_TEXT ="What time do you wake up?";
  static const String MARK_QUOTE_TEXT ="\"The secret of getting ahead is getting started.\"";
  static const String MARK_TEXT =" - Mark Twain";
  static const String NEXT_TEXT =" Next";
  static const String GOOD_MORNING_TEXT ="Good Morning!";
  static const String USER_NAME_MENU_TEXT ="Miss Martha Stewart";
  static const String HOME_TEXT ="Home";
  static const String ABOUT_US_TEXT ="About Us";
  static const String SUPPORT_US_TEXT ="Support Us";
  static const String HOW_IT_WORKS_TEXT ="How It Works";
  static const String EDIT_PROFILE_TEXT ="Edit Profile";
  static const String SUGGESTION_TEXT ="Suggestion";
  static const String JAN_DATE_TEXT ="January, 2021";
  static const String ACCOMPLISH_TOMORROW_TEXT ="Write down the 6 goals you wish to accomplish tomorrow.";
  static const String PAYMENT_INTERNET_TEXT ="Payment for Internet Bill";
  static const String SAVE_TEXT ="Save";
  static const String USER_NAME_TEXT = "Martha Stewart";
  static const String USER_DESIGNER_TEXT = "UI/UX Designer";
  static const String LATER_CHANGE_SETTING_TEXT = "You can change these setting later as well!";
  static const String WANT_SUPPORT_TEXT ="Want to support us by making a contribution?";
  static const String FUNDS_BETTER_TEXT ="Your funds will go to make the app better.";
  static const String FIVE_DOLLAR_TEXT ="\$5";
  static const String TEN_DOLLAR_TEXT ="\$10";
  static const String TWENTY_DOLLAR_TEXT ="\$20";
  static const String OTHER_TEXT ="Other";
  static const String RATING_TEXT = "Are you enjoying the O'DAAT App? \nIf so, please give us a 5-STAR rating by clicking on one of the icons.";
  static const String ABOUT_US_1_TEXT = "O'DAAT was founded by Stanford Miao in 2020 because he felt that his life had become unmanageable. With so much to do and so little time and having the desire to do everything at once, he really couldn't muster enough energy to get any of it done!";
  static const String ABOUT_US_2_TEXT = "To create better habits of success, he felt he had to be more goal-oriented and focus on the task at hand rather than multi-task. One of his mentors said, \"Accomplishing great achievements is an accumulation of smaller achievements.\"";
  static const String ABOUT_US_3_TEXT = "This is why O'DAAT was created. O'DAAT helps you organize and prioritize your goals. By focusing on one goal, and then completing it and then moving onto the next goal, helps to enforce a successful habit of staying in the present moment and being in action.";
  static const String WORKS_NUM1 ="1";
  static const String WORKS_NUM2 ="2";
  static const String WORKS_NUM3 ="3";
  static const String WORKS_NUM4 ="4";
  static const String WORKS_TEXT1 ="Set a time for when you go to bed and when wake up.";
  static const String WORKS_TEXT2 ="Before Bed write 6 goals to accomplish the following day.";
  static const String WORKS_TEXT3 ="Prioritize your 6 goals from highest priority to lowest priority.\n(1 being highest, 6 being lowest)";
  static const String WORKS_TEXT4 ="When you wake up your 1st goal will be revealed only.After you complete your goal,the next one will be revealed so you stay focused.";
  static const String PROVIDE_SUGGESTION_TEXT ="Please Provide Suggestions for further improvement.";
  static const String SUBJECT_TEXT ="Subject:";
  static const String MESSAGE_TEXT ="Message:";
  static const String YOUR_TEXT ="Your ";
  static const String GOAL_TEXT =" goal of the day is";
  static const String FIRST_TEXT ="1st";
  static const String SECOND_TEXT ="2nd";
  static const String PAUSE_TEXT = "Pause";
  static const String START_TEXT = "Start";
  static const String STOP_TEXT = "Stop";
  static const String COMPLETE_TEXT = "Complete";
  static const String HANG_ON_TEXT = "Hang On!";
  static const String COMPLETED_GOAL_TEXT = "Are you sure you completed your goal?";
  static const String YES_TEXT = "Yes";
  static const String NO_TEXT = "No";
  static const String OK_TEXT = "OK";
  static const String CONGRATULATIONS_TEXT ="Congratulations ! You did it !";
  static const String ADD_GOAL_TEXT = "Add Goal!";
  static const String EDIT_GOAL_TEXT = "Edit Goal!";
  static const String ADD_TEXT = "Add";
  static const String PAY_TEXT = "Pay";
  static const String GOAL_SHOW_TEXT = "Goal";
  static const String SHOW_GOAL_STATUS_TEXT ="Show Goal Status";
  static const String ALERT_TEXT = "Alert!";
  static const String DELETE_ASK_TEXT = "Are you sure you want to delete this goal?";
  static const String REMINDER_TEXT = "Reminder!";
  static const String SAVE_GOAL_TEXT = "Once you save goal you will not be able to reorganize them.Are you still sure you want to save goals?";
  static const String GOAL_ADD_CONGRATULATIONS_TEXT = "Congratulations! You’re on your path to great success! You’ve written down your 6 goals for tomorrow.";
  static const String EDIT_PROFILE_TOAST_TEXT = "Please Select All Fields";
  static const String GOAL_ERROR_TEXT = "Please write your Sleeping and Wake Up Time in edit profile before adding goals.";
  static const String TAP_CONTINUE_TEXT = "Press to Continue";
  static const String CAMERA_TEXT = "Camera";
  static const String GALLERY_TEXT = "Gallery";
  static const String GOAL_STARTED_TEXT = "Goal Started";
  static const String GOAL_COMPLETED_TEXT = "Goal Updated";





  // ----------------------- Hint Messages --------------------- //
  static const String NAME_HINT ="Name";
  static const String EMAIL_HINT ="E-Mail";
  static const String SLEEPING_TIME_HINT ="Sleeping Time";
  static const String WAKE_UP_TIME_HINT ="Wake Up Time";
  static const String SUBJECT_HINT ="Subject";
  static const String MESSAGE_HINT ="Message";
  static const String GOAL_Message_HINT ="Write Goal Message ";
  static const String ENTER_AMOUNT_HINT ="Enter Amount";
  static const String GOAL_TITLE_HINT ="Write Goal Title";

  // ----------------------- Error Messages --------------------- //
  static const String NAME_ERROR = "Name can\'t be empty.";
  static const String EMAIL_ERROR = "E-Mail can\'t be empty.";
  static const String SLEEPING_TIME_ERROR = "Sleeping Time can\'t be empty.";
  static const String WAKE_UP_ERROR = "Wake Up Time can\'t be empty.";
  static const String GOALS_EMPTY_ERROR = "Goals can\'t be empty";



  //------------------- Stripe Data -----------------------//
  static const String API_BASE = "https://api.stripe.com/v1";
  //static String PUBLISHABLE_KEY = "pk_test_51IUta8BoGxBqZ18hfRfkz00zxBXVw7XSsPKqd1aWruVQW4NCWZuZWjtbOsDwTWUwAINb0020LSQoXFKSFTHEfjU800aGpYA0e0";
  // static String PUBLISHABLE_KEY = "pk_test_e8kdPoCB9k2L1h4VQBqDciEv00xFXck0OZ";
  static String PUBLISHABLE_KEY = "pk_live_JXcQKbUqQ3CpNzpOneowiPYZ";
  //static String SECRET_KEY = "sk_test_51IUta8BoGxBqZ18hhnzxsurjiQyGq1jAt4SM0yr6HDTUWukRa1UALhH8a2n45dOlUTc0MGCtiBzjEGOCxgpE4mb000WmmNFk6q";
  //static String SECRET_KEY = "sk_test_imnGkozCPoXqVfXJ4CgH2bxn00ii2EEIwI";
  static String SECRET_KEY = "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";
  static const String TRANSACTION_SUCCESSFULL = "Thank You for your Contribution";
  static const String TRANSACTION_FAILED = "Payment Failed";
  static const String TRANSACTION_CANCELLED = "Payment Cancelled";
  static const String WENT_WRONG = "Something Went Wrong";
  static const String PLEASE_ENTER_AMOUNT = "Please Enter Amount";



}