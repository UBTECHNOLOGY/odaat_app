class AssetPaths
{
  //Images
  static const String SPLASH_BACKGROUND_IMAGE = "assets/images/splash_background.png";
  static const String SPLASH_FOREGROUND_IMAGE = "assets/images/splash_foreground_logo.png";
  static const String USER_PROFILE_MENU_IMAGE = "assets/images/user_profile.png";
  static const String SUPPORT_US_IMAGE = "assets/images/support_us.png";
  static const String ANDROID_STORE_IMAGE = "assets/images/andriod_store.png";
  static const String APPLE_STORE_IMAGE = "assets/images/apple_store.png";
  static const String ABOUT_US_IMAGE = "assets/images/about_us.png";
  static const String HOW_IT_WORKS_IMAGE = "assets/images/how_it_works.png";
  static const String COMPLETE_IMAGE = "assets/images/complete.png";
  static const String DONE_IMAGE = "assets/images/done.png";
  static const String PAUSE_IMAGE = "assets/images/pause.png";


//Icons
  static const String MORE_ICON = "assets/icons/more_icon.png";
  static const String HOME_MENU_ICON = "assets/icons/home_icon.png";
  static const String ABOUT_US_MENU_ICON = "assets/icons/about_us_icon.png";
  static const String SUPPORT_US_MENU_ICON = "assets/icons/support_us_icon.png";
  static const String EDIT_PROFILE_MENU_ICON = "assets/icons/edit_profile_icon.png";
  static const String SUGGESTION_MENU_ICON = "assets/icons/suggestion_icon.png";
  static const String NEXT_MENU_ICON = "assets/icons/next_icon.png";
  static const String CAMERA_ICON = "assets/icons/camera_icon.png";

}