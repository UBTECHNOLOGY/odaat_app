
import 'package:shared_preferences/shared_preferences.dart';


class SharedPreference {
  static SharedPreference _sharedPreferenceHelper;
  static SharedPreferences _sharedPreferences;

  SharedPreference._createInstance();

  factory SharedPreference() {
    // factory with constructor, return some value
    if (_sharedPreferenceHelper == null) {
      _sharedPreferenceHelper = SharedPreference
          ._createInstance(); // This is executed only once, singleton object
    }
    return _sharedPreferenceHelper;
  }

  Future<SharedPreferences> get sharedPreference async {
    if (_sharedPreferences == null) {
      _sharedPreferences = await SharedPreferences.getInstance();
    }
    return _sharedPreferences;
  }

  ////////////////////// Clear Preference ///////////////////////////
  void clear() {
    _sharedPreferences.clear();
  }

  ////////////////////// User Data ///////////////////////////
  void setUser({String user}) {
    _sharedPreferences.setString("user_data", user);
  }

  String getUser() {
    return _sharedPreferences.getString("user_data");
  }

  void setSuggestion({String suggestion}) {
    _sharedPreferences.setString("user_suggestion", suggestion);
  }

  String getSuggestion() {
    return _sharedPreferences.getString("user_suggestion");
  }


  void setInitialAppInstall({bool initialOpen}) {
    _sharedPreferences.setBool("initial_open", initialOpen);
  }

  bool getInitialAppInstall() {
    return _sharedPreferences.getBool("initial_open");
  }


  ////////////////////// Goal List Data ///////////////////////////
  void setGoalList({String goalList}) {
    _sharedPreferences.setString("goal_list",goalList);
  }

  String getGoalList() {
    return _sharedPreferences.getString("goal_list");
  }


////////////////////// Goal Index ///////////////////////////
  void setCurrentGoalIndex({int index}) {
    _sharedPreferences.setInt("current_goal_index",index);
  }

  int getCurrentGoalIndex() {
    return _sharedPreferences.getInt("current_goal_index");
  }

////////////////////// Goal Index ///////////////////////////
  void setGoalDate({String date}) {
    _sharedPreferences.setString("goal_date",date);
  }

  String getGoalDate() {
    return _sharedPreferences.getString("goal_date");
  }

}