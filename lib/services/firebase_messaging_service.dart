import 'package:firebase_messaging/firebase_messaging.dart';

class FirebaseMessagingService {
  static FirebaseMessagingService _messagingService;

  static FirebaseMessaging _firebaseMessaging;

  FirebaseMessagingService._createInstance();

  factory FirebaseMessagingService() {
    // factory with constructor, return some value
    if (_messagingService == null) {
      _messagingService = FirebaseMessagingService
          ._createInstance(); // This is executed only once, singleton object

      _firebaseMessaging = _getMessagingService();
    }
    return _messagingService;
  }

  static FirebaseMessaging _getMessagingService() {
    return _firebaseMessaging ?? FirebaseMessaging.instance;
  }

  Future<String> getToken() {
    return _firebaseMessaging.getToken();
  }
}
