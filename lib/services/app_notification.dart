import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:odaat_app/services/shared_preference.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart';
class AppNotification{

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings initializationSettingsAndroid;
  IOSInitializationSettings initializationSettingsIOS;
  InitializationSettings initializationSettings;
  AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'odaat_id1', 'odaat app', channelDescription: 'odaat for clock app',
      importance: Importance.max,
      priority: Priority.high,
      showWhen: false,
      ticker: 'ticker');

  NotificationDetails platformChannelSpecifics;
  String wakeUpTime;
  int scheduleHour,scheduleMinute;


  void initializeNotification() async
  {
    initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
    initializationSettingsIOS = IOSInitializationSettings(
        requestSoundPermission: false,
        requestBadgePermission: false,
        requestAlertPermission: false,
        onDidReceiveLocalNotification: onpreviousIosVersionTapNotification);

    initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS);

    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onTapNotification);


    //For ios and mac os only
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );

//    final MacOSInitializationSettings initializationSettingsMacOS =
//    MacOSInitializationSettings();


  }

  //For ios version less than 10
  Future onpreviousIosVersionTapNotification(int id, String title, String body, String payload)
  {

  }


  //For android and ios version greater than 10
  Future onTapNotification(String data)
  {

  }


  Future<void> addedGoalNotification({String title,String body}) async
  {

    platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        1, title, body, platformChannelSpecifics,
        payload: 'item x');
  }


  Future<void> scheduleGoalNotification() async
  {

    await flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        'Alert',
        'Please Accomplished today Goals',
        nextInstanceOfTenAMLastYear(),
         NotificationDetails(
          android: androidPlatformChannelSpecifics
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.time);
  }

  tz.TZDateTime nextInstanceOfTenAMLastYear() {

    if(SharedPreference().getUser() != null) {

      wakeUpTime = jsonDecode(SharedPreference().getUser())["wakeUpTime"];

      scheduleHour = int.parse(wakeUpTime.substring(0,2));
      scheduleMinute = int.parse(wakeUpTime.substring(3,5));
      print("Schedule Hour:${scheduleHour}");
      print("Schedule Minutes:${scheduleMinute}");

      initializeTimeZones();
      final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
      print("Schedule Date:${now}");
      return tz.TZDateTime(tz.local, now.year, now.month, now.day, scheduleHour,scheduleMinute);
    }
      }









}
